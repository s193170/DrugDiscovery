from __future__ import print_function

import networkx as nx
import argparse
import multiprocessing
from rdkit import Chem

NUM_PROCESSES = 4


def get_arguments():
    parser = argparse.ArgumentParser(description='Convert an rdkit Mol to nx graph, preserving chemical attributes')
    parser.add_argument('smiles', type=str,
                        help='The input file containing SMILES strings representing an input molecules.')
    parser.add_argument('nx_pickle', type=str, help='The output file containing sequence of pickled nx graphs')
    # parser.add_argument('nodes', type=str, help='The number of atoms to be controlled')

    parser.add_argument('--num_processes', type=int, default=NUM_PROCESSES,
                        help='The number of concurrent processes to use when converting.')
    return parser.parse_args()


def mol_to_nx(mol):
    G = nx.Graph()
    atom_list = mol.GetAtoms()

    # Checking if it has only 4 types of atoms C, H, O, N

    for i in range(len(atom_list)):
        atom = atom_list[i]
        atomic_number = atom.GetAtomicNum()
        if atomic_number == 8 or atomic_number == 1 or atomic_number == 6 or atomic_number == 7:
            continue
        else:
            print("Error")
            return ""
    for i in range(len(atom_list)):
        atom = atom_list[i]
        pos = mol.GetConformer().GetAtomPosition(i)
        G.add_node(atom.GetIdx(),
                   atomic_num=atom.GetAtomicNum(),
                   formal_charge=atom.GetFormalCharge(),
                   chiral_tag=atom.GetChiralTag(),
                   hybridization=atom.GetHybridization(),
                   num_explicit_hs=atom.GetNumExplicitHs(),
                   is_aromatic=atom.GetIsAromatic(),
                   coord=(pos.x, pos.y, pos.z)
                   )
    for bond in mol.GetBonds():
        G.add_edge(bond.GetBeginAtomIdx(),
                   bond.GetEndAtomIdx(),
                   bond_type=bond.GetBondType())
    return G




def do_all(block, validate=False):
    try:np
        mol = Chem.rdmolfiles.MolFromMol2Block(block, removeHs=False)
        mol1 = Chem.AddHs(mol)
        # mol = Chem.MolFromSmiles(smiles.strip())
        # can_smi = Chem.MolToSmiles(mol1)
        G = mol_to_nx(mol1)
    except:
        G = ""
    # if validate:
    #    mol = nx_to_mol(G)
    #    new_smi = Chem.MolToSmiles(mol)
    #    assert new_smi == smiles
    return G


def main():
    args = get_arguments()
    f = open(args.smiles)
    molecules = []
    
    for line in f:
        molecules.append(line.strip())
    print(molecules)
    # print(molecules)
    p = multiprocessing.Pool(args.num_processes)
    # results = do_all(molecules)
    # print("Results", len(results))
    results = p.map(do_all, molecules)

    for result in results:
        print(result)


    list_G = []
    # print(nx.number_of_nodes(results[0]))
    o = open(args.nx_pickle, 'w')

    # if nx.number_of_nodes(results) == 30:
    #         list_G.append(results)
    # '''
    for result in results:
        if result != "":
            list_G.append(result)
            # if nx.number_of_nodes(result) == int(args.nodes):
    # '''
    nx.write_gpickle(list_G, o)
    # if nx.number_of_nodes(result) <= 50:
    #	nx.write_gpickle(result, o)nx.write_gpickle(result, o)
    #	#nx.write_gpickle(result, o)
    o.close()


if __name__ == '__main__':
    main()