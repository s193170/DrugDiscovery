import torch
from torch import nn
import torch_geometric


class NeVAEConv(torch_geometric.nn.conv.MessagePassing):
    # https://pytorch-geometric.readthedocs.io/en/latest/modules/nn.html#torch_geometric.nn.conv.GraphConv
    # The NeVAE as in their implementation is like GraphConv, but where product is used instead of sum,
    #  It would probably perform better if GraphConv was used instead, along with a normal convolutional architecture.
    #   As for the weights based on bond type, yeah, either multiply some scalar, or use a separate set of Theta2.
    #    The second option may run into issues from a lack of data, since single bonds are by far the most common.
    #    A fix may be to one-hot-encode the bond type, and add that to the features that Theta2 is multiplied with!

    # It is claimed in the paper that this generalizes the GraphSage, but it really doesn't, as the aggregation is not
    #  sum, but its a "pairwise product".

    # Some observations:
    # Note the graph conv with aggr=add can be seen as x' = [Theta1, Theta2]@[x_i; add x_j], which makes sense: it is a
    # linear combination of the current state, and the sum of the others. The case where aggr=mean is similar, but with
    # a factor 1/N multiplied on, in our case this does probably not make sense - remember carbon should always have 4
    # neighbours except if there are double/triple bonds - but we also remove hydrogens from the graph.
    #  But this hints at a possibility: use aggr add, but apply a weighted sum with a constant vector, representing the
    #   hydrogens, e.g. CH4 should have 100% constant hydrogen vector, CH3-CH3 should have 75% h-vector and 25% neighbo
    #   -our message vector. (One can also simply not remove hydrogens from the graph. If positional features are not
    #    used, this would give a similar result for one messaging update step.) What if bias=True.
    # Using aggr=max means that gradients will only flow through one (of the e.g. four) neighbours.

    def __init__(self, in_channels, out_channels, bias=True, aggr='add', **kwargs):
        super().__init__(aggr=aggr, **kwargs)
        self.linear = nn.Linear(in_channels, out_channels, bias=bias)

    def reset_parameters(self):
        self.linear.reset_parameters()

    def forward(self, x, edge_index, edge_weight=None, size=None):
        x = self.linear(x)
        # edge_weight is the yuv, x are the features
        return self.propagate(edge_index, size=size, x=x, edge_weight=edge_weight)

    def message(self, x_j, edge_weight):
        # in case we had a self.g = nn.Identity, it should be applied on x_j here. But using g does not make sense.
        return x_j if edge_weight is None else edge_weight.view(-1, 1) * x_j

    def update(self, aggr_out, x):
        # Why "pairwise product"?? Does not seem numerically stable.
        #  Especially, if there is no neighbours, then the null-sum aggregation is 0 (i assume), so the own-features will be set to zero.
        #  Also in the paper it says this generalizes sageconv, but it doesn't really, as sageconv uses sum and not product.
        return aggr_out * x

class ProbabilisticEncoderNode(nn.Module):
    def __init__(self, features, hidden_size, K=1):
        super().__init__()
        # We could just try just a normal conv architecture e.g. with dropout, batch-norm, activation, residual connections, ..., and varying hidden sizes.
        # An idea could be to do pooling if possible. (E.g. just experiment with _geometric.nn pooling layers)
        #self.activation = nn.Identity  # = r, The activation function.

        # The first conv is special, as that only involves the node itself, and not the neighbours.
        #  I.e. this corresponds to a Conv2D with kernel size (1, 1), while the rest correspond to kernel size (3, 3).
        self.c1 = nn.Linear(features, hidden_size) # r(FN(x))
        self.cn = nn.ModuleList(NeVAEConv(hidden_size, hidden_size) for _ in range(K-1))  # r( FN(x) * aggr FN(y) )

    def reset_parameters(self):
        self.c1.reset_parameters()
        for item in self.cn:
            item.reset_parameters()

    def forward(self, x, edge_index, edge_attr, u, batch):
        result = [self.c1(x)]
        for conv in self.cn:
            result.append(conv(result[-1], edge_index, edge_attr))
        return torch.cat(result, 1)

class ProbabilisticEncoderGlobal(nn.Module):
    def __init__(self, hidden_size, latent_size):
        super().__init__()
        # Usually you would use an exponential on the output, but i guess softplus works?
        #  (ln(1+exp(x)) instead of exp(x).) It constrains the output to always be positive.
        #  But why do that for the mu's? Also why use an output function at all for mu's?
        # Also softplus is not 1 in 0..
        activation = nn.Softplus
        self.network = nn.Sequential(nn.Linear(hidden_size, hidden_size), activation(),
                                     nn.Linear(hidden_size, latent_size * 2), activation())

    def forward(self, x, edge_index, edge_attr, u, batch):
        return self.network(x)  # Half of the output is mu, the other half is sigma.



class NeVAE(nn.Module):
    def __init__(self, input_size=4, hidden_size=56, latent_size=5, output_size=5, K=5):
        super().__init__()
        self.encoder = torch_geometric.nn.MetaLayer(node_model=ProbabilisticEncoderNode(input_size, hidden_size, K=K),
                                                    global_model=ProbabilisticEncoderGlobal(hidden_size*K, latent_size))
        self.decoder = nn.Sequential(nn.Linear(latent_size, output_size), nn.Softplus())  # Softplus on output, hmm...

    torch.distributions.poisson.Poisson(2)
    def forward(self, x, edge_index, edge_attr):
        x, _, _ = self.encoder(x, edge_index, edge_attr)
        mu, sigma = torch.chunk(x, 2, -1)
        # Reparaemeterization trick
        epsilon = torch.empty_like(mu)
        epsilon.normal_()
        z = epsilon * sigma + mu
        print(z)
        print()
        return


if __name__ == "__main__":
    from rdkit import Chem
    m = Chem.MolFromSmiles('c1ccccc1')

    adjmat = torch.as_tensor(Chem.GetAdjacencyMatrix(m, useBO=True), dtype=torch.float)
    edge_index, edge_weight = torch_geometric.utils.dense_to_sparse(adjmat)
    # from the nevae code, the edge weight is actually the bond type, i.e. 1, 1.5, 2, 3, ...
    # it should really just be a one-hot featureized vector, which would be applied in the messaging step.

    atoms = ['C', 'H', 'O', 'N']
    atoms = {C:i for i, C in enumerate(atoms)}

    nodes = torch.as_tensor( [atoms[atom.GetSymbol()] for atom in m.GetAtoms()], dtype=torch.long )
    x = nn.functional.one_hot(nodes, num_classes=len(atoms)).float()
    print(x)
    nevae = NeVAE()
    print(x.shape, edge_index.shape, edge_weight.shape)
    result = nevae(x, edge_index, edge_weight)
    print(result)

    exit()
    from torchviz import make_dot
    from graphviz import Digraph
    g = make_dot(result)
    g.render("attached", format="png")

