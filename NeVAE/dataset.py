
import os.path as osp
from rdkit import Chem
from rdkit.Chem import AllChem
import torch
from torch_geometric.data import Dataset
import networkx as nx
import torch.nn as nn
import torch
import torch_geometric
import numpy as np



# Convert smile to desired format
def smile_conv(smile):
    m = Chem.MolFromSmiles(smile)
    # m = Chem.AddHs(m) - Comment out for now

    n = len(m.GetAtoms())

    # Adjecency matrix  and weights
    adjmat = torch.as_tensor(Chem.GetAdjacencyMatrix(m, useBO=True), dtype=torch.float)
    edge_index, edge_weights = torch_geometric.utils.dense_to_sparse(adjmat)


    # One-hot encode
    atoms = ['C', 'H', 'O', 'N']
    atoms = {C: i for i, C in enumerate(atoms)}
    nodes = torch.as_tensor([atoms[atom.GetSymbol()] for atom in m.GetAtoms()], dtype=torch.long)
    x = nn.functional.one_hot(nodes, num_classes=len(atoms)).float()

    # Get coordinates
    AllChem.Compute2DCoords(m)
    pos = torch.as_tensor(((n, 3)))

    for i in range(n):
        p = m.GetConformer().GetAtomPosition(i)
        pos[i, :] = [p.x, p.y, p.z]

    # Return features, edge index, edge weight and position
    return (x,edge_index,edge_weights,pos)

