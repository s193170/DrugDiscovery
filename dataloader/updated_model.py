import os
import sys
import numpy as np
import math
from time import time

import torch
import torch.nn as nn
import torch.nn.functional as F

from MaskGAF import MaskedGraphAF

from rdkit import Chem
import environment as env
# from environment import check_valency, convert_radical_electrons_to_hydrogens
from utils import save_one_mol, save_one_reward

class ST_Net_Sigmoid(nn.Module):
    def __init__(self, args, input_dim, output_dim, hid_dim=64, num_layers=2, bias=True, scale_weight_norm=False, sigmoid_shift=2., apply_batch_norm=False):
        super(ST_Net_Sigmoid, self).__init__()
        self.num_layers = num_layers  # unused
        self.args = args
        self.input_dim = input_dim
        self.hid_dim = hid_dim
        self.output_dim = output_dim
        self.bias = bias
        self.apply_batch_norm = apply_batch_norm
        self.scale_weight_norm = scale_weight_norm
        self.sigmoid_shift = sigmoid_shift

        self.linear1 = nn.Linear(input_dim, hid_dim, bias=bias)
        self.linear2 = nn.Linear(hid_dim, output_dim*2, bias=bias)

        if self.apply_batch_norm:
            self.bn_before = nn.BatchNorm1d(input_dim)
        if self.scale_weight_norm:
            self.rescale1 = nn.utils.weight_norm(Rescale())
            self.rescale2 = nn.utils.weight_norm(Rescale())

        else:
            self.rescale1 = Rescale()
            self.rescale2 = Rescale()

        self.tanh = nn.Tanh()
        self.sigmoid = nn.Sigmoid()
        self.reset_parameters()

    def reset_parameters(self):
        nn.init.xavier_uniform_(self.linear1.weight)
        nn.init.constant_(self.linear2.weight, 1e-10)
        if self.bias:
            nn.init.constant_(self.linear1.bias, 0.)
            nn.init.constant_(self.linear2.bias, 0.)

    def forward(self, x):
        '''
        :param x: (batch * repeat_num for node/edge, emb)
        :return: w and b for affine operation
        '''
        if self.apply_batch_norm:
            x = self.bn_before(x)

        x = self.linear2(self.tanh(self.linear1(x)))
        x = self.rescale1(x)
        s = x[:, :self.output_dim]
        t = x[:, self.output_dim:]
        s = self.sigmoid(s + self.sigmoid_shift)
        s = self.rescale2(s) # linear scale seems important, similar to learnable prior..
        return s, t


node_st_net = nn.ModuleList([ST_Net_Sigmoid(self.args, nout, self.num_node_type, hid_dim=nhid, bias=True,
                                            scale_weight_norm=self.args.scale_weight_norm,
                                            sigmoid_shift=self.args.sigmoid_shift,
                                            apply_batch_norm=self.args.is_bn_before) for i in range(5)])

edge_st_net = nn.ModuleList([ST_Net_Sigmoid(self.args, nout * 3, self.num_edge_type, hid_dim=nhid, bias=True,
                                            scale_weight_norm=self.args.scale_weight_norm,
                                            sigmoid_shift=self.args.sigmoid_shift,
                                            apply_batch_norm=self.args.is_bn_before) for i in range(5)])


class Rescale(nn.Module):
    def __init__(self):
        super(Rescale, self).__init__()
        self.weight = nn.Parameter(torch.zeros([1]))

    def forward(self, x):
        if torch.isnan(torch.exp(self.weight)).any():
            print(self.weight)
            raise RuntimeError('Rescale factor has NaN entries')

        x = torch.exp(self.weight) * x
        return x


class Rescale_channel(nn.Module):
    def __init__(self, num_channels):
        super(Rescale_channel, self).__init__()
        self.num_channels = num_channels
        self.weight = nn.Parameter(torch.zeros([num_channels]))

    def forward(self, x):
        if torch.isnan(torch.exp(self.weight)).any():
            raise RuntimeError('Rescale factor has NaN entries')

        x = torch.exp(self.weight) * x
        return x
