# coding=utf-8
"""
Anonymous author
"""
import os
import sys
import numpy as np
import math
from time import time

import torch
import torch.nn as nn
import torch.nn.functional as F

from MaskGAF import MaskedGraphAF

from rdkit import Chem
import environment as env
# from environment import check_valency, convert_radical_electrons_to_hydrogens
from utils import save_one_mol, save_one_reward


class GraphFlowModel(nn.Module):
    """
    Reminder:
        self.args: deq_coeff
                   deq_type

    Args:
    Returns:

    """

    def __init__(self, max_size, node_dim, bond_dim, edge_unroll, args):
        super(GraphFlowModel, self).__init__()
        self.max_size = max_size
        self.node_dim = node_dim
        self.bond_dim = bond_dim
        self.edge_unroll = edge_unroll
        self.args = args

        ###Flow hyper-paramters
        self.num_flow_layer = self.args.num_flow_layer
        self.nhid = self.args.nhid
        self.nout = self.args.nout

        self.node_masks = None
        if self.node_masks is None:
            self.node_masks, self.adj_masks, \
            self.link_prediction_index, self.flow_core_edge_masks = self.initialize_masks(max_node_unroll=self.max_size,
                                                                                          max_edge_unroll=self.edge_unroll)

        self.latent_step = self.node_masks.size(0)  # (max_size) + (max_edge_unroll - 1) / 2 * max_edge_unroll + (max_size - max_edge_unroll) * max_edge_unroll
        self.latent_node_length = self.max_size * self.node_dim
        self.latent_edge_length = (self.latent_step - self.max_size) * self.bond_dim
        print('latent node length: %d' % self.latent_node_length)
        print('latent edge length: %d' % self.latent_edge_length)

        self.constant_pi = nn.Parameter(torch.Tensor([3.1415926535]), requires_grad=False)
        # learnable
        if self.args.learn_prior:
            self.prior_ln_var = nn.Parameter(torch.zeros([1]))  # log(1^2) = 0
            nn.init.constant_(self.prior_ln_var, 0.)
        else:
            self.prior_ln_var = nn.Parameter(torch.zeros([1]), requires_grad=False)

        self.dp = False
        num_gpus = torch.cuda.device_count()
        if num_gpus >= 1:
            self.dp = True
            print('using %d GPUs' % num_gpus)

        self.flow_core = MaskedGraphAF(self.node_masks, self.adj_masks,
                                       self.link_prediction_index,
                                       num_flow_layer=self.num_flow_layer,
                                       graph_size=self.max_size,
                                       num_node_type=self.node_dim,
                                       num_edge_type=self.bond_dim,
                                       args=self.args,
                                       nhid=self.nhid,
                                       nout=self.nout)
        if self.dp:
            self.flow_core = nn.DataParallel(self.flow_core)

    def forward(self, inp_node_features, inp_adj_features):
        """
        Args:
            inp_node_features: (B, N, 9)
            inp_adj_features: (B, 4, N, N)

        Returns:
            z: [(B, node_num*9), (B, edge_num*4)]
            logdet:  ([B], [B])
        """
        # inp_node_features_cont = inp_node_features #(B, N, 9) #! this is buggy. shallow copy
        inp_node_features_cont = inp_node_features.clone()  # (B, N, 9)

        inp_adj_features_cont = inp_adj_features[:, :, self.flow_core_edge_masks].clone()  # (B, 4, edge_num)
        inp_adj_features_cont = inp_adj_features_cont.permute(0, 2, 1).contiguous()  # (B, edge_num, 4)

        if self.args.deq_type == 'random':
            # TODO: put the randomness on GPU.!
            inp_node_features_cont += self.args.deq_coeff * torch.rand(
                inp_node_features_cont.size()).cuda()  # (B, N, 9)
            inp_adj_features_cont += self.args.deq_coeff * torch.rand(
                inp_adj_features_cont.size()).cuda()  # (B, edge_num, 4)

        elif self.args.deq_type == 'variational':
            raise ValueError('current unsupported method: %s' % self.args.deq_type)
        else:
            raise ValueError('unsupported dequantization type (%s)' % self.args.deq_type)

        z, logdet = self.flow_core(inp_node_features, inp_adj_features,
                                   inp_node_features_cont, inp_adj_features_cont)

        if self.args.deq_type == 'random':
            return z, logdet, self.prior_ln_var


    def generate_one_molecule_with_latent_provided(self, latent_manipulated, latent_org, max_step, max_atoms=48,
                                                   temperature=0.75):
        """
        Generated a molecule with initial latent provided.
        If latent violate the valency, resample latent.
        """
        generate_start_t = time()
        max_step = np.random.randint(23)
        with torch.no_grad():
            num2bond = {0: Chem.rdchem.BondType.SINGLE, 1: Chem.rdchem.BondType.DOUBLE, 2: Chem.rdchem.BondType.TRIPLE}
            num2bond_symbol = {0: '=', 1: '==', 2: '==='}
            # [6, 7, 8, 9, 15, 16, 17, 35, 53, 0]
            num2atom = {0: 6, 1: 7, 2: 8, 3: 9, 4: 15, 5: 16, 6: 17, 7: 35, 8: 53}
            num2symbol = {0: 'C', 1: 'N', 2: 'O', 3: 'F', 4: 'P', 5: 'S', 6: 'Cl', 7: 'Br', 8: 'I'}

            prior_node_dist = torch.distributions.normal.Normal(torch.zeros([self.node_dim]).cuda(),
                                                                temperature * torch.ones([self.node_dim]).cuda())
            prior_edge_dist = torch.distributions.normal.Normal(torch.zeros([self.bond_dim]).cuda(),
                                                                temperature * torch.ones([self.bond_dim]).cuda())
            org_latent_node = latent_org[:342]
            org_latent_edge = latent_org[342:]
            m_latent_node = latent_manipulated[:342]  # (self.max_size * 9)
            m_latent_edge = latent_manipulated[342:]  # (latent_size - self.max_size * 9)
            assert m_latent_node.size(0) == (self.max_size * 9)
            assert m_latent_edge.size(0) == ((11 * 6 + (38 - 12) * 12)) * 4
            cur_node_features = torch.zeros([1, max_atoms, self.node_dim]).cuda()
            cur_adj_features = torch.zeros([1, self.bond_dim, max_atoms, max_atoms]).cuda()



            rw_mol = Chem.RWMol()  # editable mol
            mol = None

            total_edge_index = -1

            is_continue = True
            total_resample = 0
            each_node_resample = np.zeros([max_atoms])
            for i in range(max_atoms):
                if not is_continue:
                    break
                if i < self.edge_unroll:
                    edge_total = i  # edge to sample for current node
                    start = 0
                else:
                    edge_total = self.edge_unroll
                    start = i - self.edge_unroll
                # first generate node
                ## reverse flow
                if i < self.max_size and i < max_step:  # node less than 38, use pre latent
                    latent_node = org_latent_node[self.node_dim * i: self.node_dim * (i + 1)].clone().view(1,
                                                                                                           -1)  # (1, 9)
                elif i < self.max_size and i >= max_step:
                    latent_node = m_latent_node[self.node_dim * i: self.node_dim * (i + 1)].clone().view(1,
                                                                                                         -1)  # (1, 9)
                    # latent_node += 0.5 * prior_node_dist.sample().view(1, -1) #(1, 9)

                    # print('using predefined node latent for node %d' % i)
                else:
                    print('sample node latent for node %d' % i)
                    latent_node = prior_node_dist.sample().view(1, -1)  # (1, 9)

                if self.dp:
                    latent_node = self.flow_core.module.reverse(cur_node_features, cur_adj_features,
                                                                latent_node, mode=0).view(-1)  # (9, )
                else:
                    latent_node = self.flow_core.reverse(cur_node_features, cur_adj_features,
                                                         latent_node, mode=0).view(-1)  # (9, )
                ## node/adj postprocessing
                # print(latent_node.shape) #(38, 9)
                feature_id = torch.argmax(latent_node).item()
                # print(num2symbol[feature_id])
                cur_node_features[0, i, feature_id] = 1.0
                cur_adj_features[0, :, i, i] = 1.0
                rw_mol.AddAtom(Chem.Atom(num2atom[feature_id]))

                # then generate edges
                if i == 0:
                    is_connect = True
                else:
                    is_connect = False
                # cur_mol_size = mol.GetNumAtoms
                for j in range(edge_total):
                    first = True
                    total_edge_index += 1
                    valid = False
                    resample_edge = 0
                    invalid_bond_type_set = set()
                    while not valid:
                        # TODO: add cache. Some atom can not get the right edge type and is stuck in the loop
                        # TODO: add cache. invalid bond set
                        if len(
                                invalid_bond_type_set) < 3 and resample_edge <= 50:  # haven't sampled all possible bond type or is not stuck in the loop
                            if first:
                                first = False
                                if i < self.max_size and i < max_step:
                                    latent_edge = org_latent_edge[self.bond_dim * total_edge_index: self.bond_dim * (
                                                total_edge_index + 1)].clone().view(1, -1)  # (1, 4)
                                elif i < self.max_size and i >= max_step:
                                    latent_edge = m_latent_edge[self.bond_dim * total_edge_index: self.bond_dim * (
                                                total_edge_index + 1)].clone().view(1, -1)  # (1, 4)
                                    # latent_edge += 0.5 * prior_edge_dist.sample().view(1, -1) #(1, 4)
                                else:
                                    print('sample edge latent for edge %d' % total_edge_index)
                                    latent_edge = prior_edge_dist.sample().view(1, -1)  # (1, 4)
                            else:
                                print('did not use predefined edge latent for generating %d-th edge' % total_edge_index)
                                latent_edge = prior_edge_dist.sample().view(1, -1)  # (1, 4)

                            if self.dp:
                                latent_edge = self.flow_core.module.reverse(cur_node_features, cur_adj_features,
                                                                            latent_edge,
                                                                            mode=1, edge_index=torch.Tensor(
                                        [[j + start, i]]).long().cuda()).view(-1)  # (4, )
                            else:
                                latent_edge = self.flow_core.reverse(cur_node_features, cur_adj_features, latent_edge,
                                                                     mode=1, edge_index=torch.Tensor(
                                        [[j + start, i]]).long().cuda()).view(-1)  # (4, )
                            edge_discrete_id = torch.argmax(latent_edge).item()
                        else:
                            print('have tried all possible bond type, use virtual bond.')
                            assert resample_edge > 50 or len(invalid_bond_type_set) == 3
                            edge_discrete_id = 3  # we have no choice but to choose not to add edge between (i, j+start)
                        cur_adj_features[0, edge_discrete_id, i, j + start] = 1.0
                        cur_adj_features[0, edge_discrete_id, j + start, i] = 1.0
                        if edge_discrete_id == 3:  # virtual edge
                            valid = True
                        else:  # single/double/triple bond
                            rw_mol.AddBond(i, j + start, num2bond[edge_discrete_id])
                            valid = env.check_valency(rw_mol)
                            if valid:
                                is_connect = True
                                # print(num2bond_symbol[edge_discrete_id])
                            else:  # backtrack
                                rw_mol.RemoveBond(i, j + start)
                                cur_adj_features[0, edge_discrete_id, i, j + start] = 0.0
                                cur_adj_features[0, edge_discrete_id, j + start, i] = 0.0
                                total_resample += 1.0
                                each_node_resample[i] += 1.0
                                resample_edge += 1

                                invalid_bond_type_set.add(edge_discrete_id)
                if is_connect:  # new generated node has at least one bond with previous node, do not stop generation, backup mol from rw_mol to mol
                    is_continue = True
                    mol = rw_mol.GetMol()

                else:
                    is_continue = False

            # mol = rw_mol.GetMol() # mol backup
            assert mol is not None, 'mol is None...'

            # final_valid = check_valency(mol)
            final_valid = env.check_chemical_validity(mol)
            assert final_valid is True, 'warning: use valency check during generation but the final molecule is invalid!!!'

            final_mol = env.convert_radical_electrons_to_hydrogens(mol)
            smiles = Chem.MolToSmiles(final_mol, isomericSmiles=True)
            assert '.' not in smiles, 'warning: use is_connect to check stop action, but the final molecule is disconnected!!!'

            final_mol = Chem.MolFromSmiles(smiles)

            # mol = convert_radical_electrons_to_hydrogens(mol)
            num_atoms = final_mol.GetNumAtoms()
            num_bonds = final_mol.GetNumBonds()

            pure_valid = 0
            if total_resample == 0:
                pure_valid = 1.0
            print('smiles: %s | #atoms: %d | #bonds: %d | #resample: %.5f | time: %.5f |' % (
            smiles, num_atoms, num_bonds, total_resample, time() - generate_start_t))
            return smiles, pure_valid, num_atoms

    def generate(self, temperature=0.75, mute=False, max_atoms=48, cnt=None):
        """
        inverse flow to generate molecule
        Args:
            temp: temperature of normal distributions, we sample from (0, temp^2 * I)
        """
        generate_start_t = time()
        with torch.no_grad():
            num2bond = {0: Chem.rdchem.BondType.SINGLE, 1: Chem.rdchem.BondType.DOUBLE, 2: Chem.rdchem.BondType.TRIPLE}
            num2bond_symbol = {0: '=', 1: '==', 2: '==='}
            # [6, 7, 8, 9, 15, 16, 17, 35, 53, 0]
            num2atom = {0: 6, 1: 7, 2: 8, 3: 9, 4: 15, 5: 16, 6: 17, 7: 35, 8: 53}
            num2symbol = {0: 'C', 1: 'N', 2: 'O', 3: 'F', 4: 'P', 5: 'S', 6: 'Cl', 7: 'Br', 8: 'I'}



            prior_node_dist = torch.distributions.normal.Normal(torch.zeros([self.node_dim]).cuda(),
                                                                temperature * torch.ones([self.node_dim]).cuda())
            prior_edge_dist = torch.distributions.normal.Normal(torch.zeros([self.bond_dim]).cuda(),
                                                                temperature * torch.ones([self.bond_dim]).cuda())

            cur_node_features = torch.zeros([1, max_atoms, self.node_dim]).cuda()
            cur_adj_features = torch.zeros([1, self.bond_dim, max_atoms, max_atoms]).cuda()

            rw_mol = Chem.RWMol()  # editable mol
            mol = None
            # mol_size = mol.GetNumAtoms()

            is_continue = True
            total_resample = 0
            each_node_resample = np.zeros([max_atoms])
            for i in range(max_atoms):
                if not is_continue:
                    break
                if i < self.edge_unroll:
                    edge_total = i  # edge to sample for current node
                    start = 0
                else:
                    edge_total = self.edge_unroll
                    start = i - self.edge_unroll
                # first generate node
                ## reverse flow
                latent_node = prior_node_dist.sample().view(1, -1)  # (1, 9)
                if self.dp:
                    latent_node = self.flow_core.module.reverse(cur_node_features, cur_adj_features,
                                                                latent_node, mode=0).view(-1)  # (9, )
                else:
                    latent_node = self.flow_core.reverse(cur_node_features, cur_adj_features,
                                                         latent_node, mode=0).view(-1)  # (9, )
                ## node/adj postprocessing
                # print(latent_node.shape) #(38, 9)
                feature_id = torch.argmax(latent_node).item()
                # print(num2symbol[feature_id])
                cur_node_features[0, i, feature_id] = 1.0
                cur_adj_features[0, :, i, i] = 1.0
                rw_mol.AddAtom(Chem.Atom(num2atom[feature_id]))


                # then generate edges
                if i == 0:
                    is_connect = True
                else:
                    is_connect = False
                # cur_mol_size = mol.GetNumAtoms
                for j in range(edge_total):
                    valid = False
                    resample_edge = 0
                    invalid_bond_type_set = set()
                    while not valid:
                        # TODO: add cache. Some atom can not get the right edge type and is stuck in the loop
                        # TODO: add cache. invalid bond set
                        if len(
                                invalid_bond_type_set) < 3 and resample_edge <= 50:  # haven't sampled all possible bond type or is not stuck in the loop
                            latent_edge = prior_edge_dist.sample().view(1, -1)  # (1, 4)
                            if self.dp:
                                latent_edge = self.flow_core.module.reverse(cur_node_features, cur_adj_features,
                                                                            latent_edge,
                                                                            mode=1, edge_index=torch.Tensor(
                                        [[j + start, i]]).long().cuda()).view(-1)  # (4, )
                            else:
                                latent_edge = self.flow_core.reverse(cur_node_features, cur_adj_features, latent_edge,
                                                                     mode=1, edge_index=torch.Tensor(
                                        [[j + start, i]]).long().cuda()).view(-1)  # (4, )
                            edge_discrete_id = torch.argmax(latent_edge).item()
                        else:
                            if not mute:
                                print('have tried all possible bond type, use virtual bond.')
                            assert resample_edge > 50 or len(invalid_bond_type_set) == 3
                            edge_discrete_id = 3  # we have no choice but to choose not to add edge between (i, j+start)
                        cur_adj_features[0, edge_discrete_id, i, j + start] = 1.0
                        cur_adj_features[0, edge_discrete_id, j + start, i] = 1.0
                        if edge_discrete_id == 3:  # virtual edge
                            valid = True
                        else:  # single/double/triple bond
                            rw_mol.AddBond(i, j + start, num2bond[edge_discrete_id])
                            valid = env.check_valency(rw_mol)
                            if valid:
                                is_connect = True
                                # print(num2bond_symbol[edge_discrete_id])
                            else:  # backtrack
                                rw_mol.RemoveBond(i, j + start)
                                cur_adj_features[0, edge_discrete_id, i, j + start] = 0.0
                                cur_adj_features[0, edge_discrete_id, j + start, i] = 0.0
                                total_resample += 1.0
                                each_node_resample[i] += 1.0
                                resample_edge += 1

                                invalid_bond_type_set.add(edge_discrete_id)
                if is_connect:  # new generated node has at least one bond with previous node, do not stop generation, backup mol from rw_mol to mol
                    is_continue = True
                    mol = rw_mol.GetMol()

                else:
                    is_continue = False

            # mol = rw_mol.GetMol() # mol backup
            assert mol is not None, 'mol is None...'

            # final_valid = check_valency(mol)
            final_valid = env.check_chemical_validity(mol)
            assert final_valid is True, 'warning: use valency check during generation but the final molecule is invalid!!!'

            final_mol = env.convert_radical_electrons_to_hydrogens(mol)
            smiles = Chem.MolToSmiles(final_mol, isomericSmiles=True)
            assert '.' not in smiles, 'warning: use is_connect to check stop action, but the final molecule is disconnected!!!'

            final_mol = Chem.MolFromSmiles(smiles)

            # mol = convert_radical_electrons_to_hydrogens(mol)
            num_atoms = final_mol.GetNumAtoms()
            num_bonds = final_mol.GetNumBonds()

            pure_valid = 0
            if total_resample == 0:
                pure_valid = 1.0
            if not mute:
                cnt = str(cnt) if cnt is not None else ''
                print('smiles%s: %s | #atoms: %d | #bonds: %d | #resample: %.5f | time: %.5f |' % (
                cnt, smiles, num_atoms, num_bonds, total_resample, time() - generate_start_t))
            return smiles, pure_valid, num_atoms

    def initialize_masks(self, max_node_unroll=38, max_edge_unroll=12):
        """
        Args:
            max node unroll: maximal number of nodes in molecules to be generated (default: 38)
            max edge unroll: maximal number of edges to predict for each generated nodes (default: 12, calculated from zink250K data)
        Returns:
            node_masks: node mask for each step
            adj_masks: adjacency mask for each step
            is_node_update_mask: 1 indicate this step is for updating node features
            flow_core_edge_mask: get the distributions we want to model in adjacency matrix
        """
        num_masks = int(
            max_node_unroll + (max_edge_unroll - 1) * max_edge_unroll / 2 + (max_node_unroll - max_edge_unroll) * (
                max_edge_unroll))
        num_mask_edge = int(num_masks - max_node_unroll)

        node_masks1 = torch.zeros([max_node_unroll, max_node_unroll]).byte()
        adj_masks1 = torch.zeros([max_node_unroll, max_node_unroll, max_node_unroll]).byte()
        node_masks2 = torch.zeros([num_mask_edge, max_node_unroll]).byte()
        adj_masks2 = torch.zeros([num_mask_edge, max_node_unroll, max_node_unroll]).byte()
        # is_node_update_masks = torch.zeros([num_masks]).byte()

        link_prediction_index = torch.zeros([num_mask_edge, 2]).long()

        flow_core_edge_masks = torch.zeros([max_node_unroll, max_node_unroll]).byte()

        # masks_edge = dict()
        cnt = 0
        cnt_node = 0
        cnt_edge = 0
        for i in range(max_node_unroll):
            node_masks1[cnt_node][:i] = 1
            adj_masks1[cnt_node][:i, :i] = 1
            # is_node_update_masks[cnt] = 1
            cnt += 1
            cnt_node += 1

            edge_total = 0
            if i < max_edge_unroll:
                start = 0
                edge_total = i
            else:
                start = i - max_edge_unroll
                edge_total = max_edge_unroll
            for j in range(edge_total):
                if j == 0:
                    node_masks2[cnt_edge][:i + 1] = 1
                    adj_masks2[cnt_edge] = adj_masks1[cnt_node - 1].clone()
                    adj_masks2[cnt_edge][i, i] = 1
                else:
                    node_masks2[cnt_edge][:i + 1] = 1
                    adj_masks2[cnt_edge] = adj_masks2[cnt_edge - 1].clone()
                    adj_masks2[cnt_edge][i, start + j - 1] = 1
                    adj_masks2[cnt_edge][start + j - 1, i] = 1
                cnt += 1
                cnt_edge += 1
        assert cnt == num_masks, 'masks cnt wrong'
        assert cnt_node == max_node_unroll, 'node masks cnt wrong'
        assert cnt_edge == num_mask_edge, 'edge masks cnt wrong'

        cnt = 0
        for i in range(max_node_unroll):
            if i < max_edge_unroll:
                start = 0
                edge_total = i
            else:
                start = i - max_edge_unroll
                edge_total = max_edge_unroll

            for j in range(edge_total):
                link_prediction_index[cnt][0] = start + j
                link_prediction_index[cnt][1] = i
                cnt += 1
        assert cnt == num_mask_edge, 'edge mask initialize fail'

        for i in range(max_node_unroll):
            if i == 0:
                continue
            if i < max_edge_unroll:
                start = 0
                end = i
            else:
                start = i - max_edge_unroll
                end = i
            flow_core_edge_masks[i][start:end] = 1

        node_masks = torch.cat((node_masks1, node_masks2), dim=0)
        adj_masks = torch.cat((adj_masks1, adj_masks2), dim=0)

        node_masks = nn.Parameter(node_masks, requires_grad=False)
        adj_masks = nn.Parameter(adj_masks, requires_grad=False)
        link_prediction_index = nn.Parameter(link_prediction_index, requires_grad=False)
        flow_core_edge_masks = nn.Parameter(flow_core_edge_masks, requires_grad=False)

        return node_masks, adj_masks, link_prediction_index, flow_core_edge_masks

    def log_prob(self, z, logdet, deq_logp=None, deq_logdet=None):

        logdet[0] = logdet[0] - self.latent_node_length  # calculate probability of a region from probability density, minus constant has no effect on optimization
        logdet[1] = logdet[1] - self.latent_edge_length  # calculate probability of a region from probability density, minus constant has no effect on optimization

        ll_node = -1 / 2 * (torch.log(2 * self.constant_pi) + self.prior_ln_var + torch.exp(-self.prior_ln_var) * (z[0] ** 2))
        ll_node = ll_node.sum(-1)  # (B)

        ll_edge = -1 / 2 * (torch.log(2 * self.constant_pi) + self.prior_ln_var + torch.exp(-self.prior_ln_var) * (z[1] ** 2))
        ll_edge = ll_edge.sum(-1)  # (B)

        ll_node += logdet[0]  # ([B])
        ll_edge += logdet[1]  # ([B])

        if self.args.deq_type == 'random':
            if self.args.divide_loss:
                return -(torch.mean(ll_node + ll_edge) / (self.latent_edge_length + self.latent_node_length))
            else:
                # ! useless
                return -torch.mean(ll_node + ll_edge)  # scalar


# coding: utf-8
# Anonymous author


import numpy as np
import math

import torch
import torch.nn as nn
import torch.nn.functional as F

from time import time


def Linear(in_features, out_features, bias=True):
    m = nn.Linear(in_features, out_features, bias)
    nn.init.xavier_uniform_(m.weight)
    if bias:
        nn.init.constant_(m.bias, 0.0)
    return m


class RelationGraphConvolution(nn.Module):
    """
    Relation GCN layer.
    """

    def __init__(self, in_features, out_features, edge_dim=3, aggregate='sum', dropout=0., use_relu=True, bias=False):
        '''
        :param in/out_features: scalar of channels for node embedding
        :param edge_dim: dim of edge type, virtual type not included
        '''
        super(RelationGraphConvolution, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.edge_dim = edge_dim
        self.dropout = dropout
        self.aggregate = aggregate
        if use_relu:
            self.act = nn.ReLU()
        else:
            self.act = None

        self.weight = nn.Parameter(torch.FloatTensor(
            self.edge_dim, self.in_features, self.out_features))
        if bias:
            self.bias = nn.Parameter(torch.FloatTensor(
                self.edge_dim, 1, self.out_features))
        else:
            self.register_parameter('bias', None)
        self.reset_parameters()

    def reset_parameters(self):
        nn.init.xavier_uniform_(self.weight)
        if self.bias is not None:
            nn.init.constant_(self.bias, 0.)

    def forward(self, x, adj):
        '''
        :param x: (batch, N, d)
        :param adj: (batch, E, N, N)
        typically d=9 e=3
        :return:
        updated x with shape (batch, N, d)
        '''
        x = F.dropout(x, p=self.dropout, training=self.training)  # (b, N, d)

        batch_size = x.size(0)

        # transform
        support = torch.einsum('bid, edh-> beih', x, self.weight)
        output = torch.einsum('beij, bejh-> beih', adj,
                              support)  # (batch, e, N, d)

        if self.bias is not None:
            output += self.bias
        if self.act is not None:
            output = self.act(output)  # (b, E, N, d)
        output = output.view(batch_size, self.edge_dim, x.size(
            1), self.out_features)  # (b, E, N, d)

        if self.aggregate == 'sum':
            # sum pooling #(b, N, d)
            node_embedding = torch.sum(output, dim=1, keepdim=False)
        elif self.aggregate == 'max':
            # max pooling  #(b, N, d)
            node_embedding = torch.max(output, dim=1, keepdim=False)
        elif self.aggregate == 'mean':
            # mean pooling #(b, N, d)
            node_embedding = torch.mean(output, dim=1, keepdim=False)
        elif self.aggregate == 'concat':
            # ! implementation wrong
            node_embedding = torch.cat(torch.split(
                output, dim=1, split_size_or_sections=1), dim=3)  # (b, 1, n, d*e)
            node_embedding = torch.squeeze(
                node_embedding, dim=1)  # (b, n, d*e)
        else:
            print('GCN aggregate error!')
        return node_embedding

    def __repr__(self):
        return self.__class__.__name__ + ' (' + str(self.in_features) + ' -> ' + str(self.out_features) + ')'


class RGCN(nn.Module):
    def __init__(self, nfeat, nhid=128, nout=128, edge_dim=3, num_layers=3, dropout=0., normalization=False):
        '''
        :num_layars: the number of layers in each R-GCN
        '''
        super(RGCN, self).__init__()

        self.nfeat = nfeat
        self.nhid = nhid
        self.nout = nout
        self.edge_dim = edge_dim
        self.num_layers = num_layers

        self.dropout = dropout
        self.normalization = normalization

        self.emb = Linear(nfeat, nfeat, bias=False)
        # self.bn_emb = nn.BatchNorm2d(8)

        self.gc1 = RelationGraphConvolution(
            nfeat, nhid, edge_dim=self.edge_dim, aggregate='sum', use_relu=True, dropout=self.dropout, bias=False)
        # if self.normalization:
        #    self.bn1 = nn.BatchNorm2d(nhid)

        self.gc2 = nn.ModuleList([RelationGraphConvolution(nhid, nhid, edge_dim=self.edge_dim, aggregate='sum',
                                                           use_relu=True, dropout=self.dropout, bias=False)
                                  for i in range(self.num_layers - 2)])
        # if self.normalization:
        #    self.bn2 = nn.ModuleList([nn.BatchNorm2d(nhid) for i in range(self.num_layers-2)])

        self.gc3 = RelationGraphConvolution(
            nhid, nout, edge_dim=self.edge_dim, aggregate='sum', use_relu=False, dropout=self.dropout, bias=False)
        # if self.normalization
        #    self.bn3 = nn.BatchNorm2d(nout)

    def forward(self, x, adj):
        '''
        :param x: (batch, N, d)
        :param adj: (batch, E, N, N)
        :return:
        '''
        # TODO: Add normalization for adacency matrix
        # embedding layer
        x = self.emb(x)
        # if self.normalization:
        #    x = self.bn_emb(x.transpose(0, 3, 1, 2))
        #    x = x.transpose(0, 2, 3, 1)

        # first GCN layer
        x = self.gc1(x, adj)
        # if self.normalization:
        #    x = self.bn1(x.transpose(0, 3, 1, 2))
        #    x = x.transpose(0, 2, 3, 1)

        # hidden GCN layer(s)
        for i in range(self.num_layers - 2):
            x = self.gc2[i](x, adj)  # (#node, #class)
            # if self.normalization:
            #    x = self.bn2[i](x.transpose(0, 3, 1, 2))
            #    x = x.transpose(0, 2, 3, 1)

        # last GCN layer
        x = self.gc3(x, adj)  # (batch, N, d)
        # check here: bn for last layer seem to be necessary
        # x = self.bn3(x.transpose(0, 3, 1, 2))
        # x = x.transpose(0, 2, 3, 1)

        # return node embedding
        return x


# TODO: Try sample dependent initialization.!!
# TODO: Try different st function (sigmoid, softplus, exp, spine, flow++)


class ST_Net_Exp(nn.Module):
    def __init__(self, args, input_dim, output_dim, hid_dim=64, num_layers=2, bias=True, scale_weight_norm=False,
                 sigmoid_shift=2., apply_batch_norm=False):
        super(ST_Net_Exp, self).__init__()
        self.num_layers = num_layers  # unused
        self.args = args
        self.input_dim = input_dim
        self.hid_dim = hid_dim
        self.output_dim = output_dim
        self.bias = bias
        self.apply_batch_norm = apply_batch_norm
        self.scale_weight_norm = scale_weight_norm
        self.sigmoid_shift = sigmoid_shift

        self.linear1 = nn.Linear(input_dim, hid_dim, bias=bias)
        self.linear2 = nn.Linear(hid_dim, output_dim * 2, bias=bias)

        if self.apply_batch_norm:
            self.bn_before = nn.BatchNorm1d(input_dim)
        if self.scale_weight_norm:
            self.rescale1 = nn.utils.weight_norm(Rescale())
            # self.rescale2 = nn.utils.weight_norm(Rescale())

        else:
            self.rescale1 = Rescale()
            # self.rescale2 = Rescale()

        self.tanh = nn.Tanh()
        # self.sigmoid = nn.Sigmoid()
        self.reset_parameters()

    def reset_parameters(self):
        nn.init.xavier_uniform_(self.linear1.weight)
        nn.init.constant_(self.linear2.weight, 1e-10)
        if self.bias:
            nn.init.constant_(self.linear1.bias, 0.)
            nn.init.constant_(self.linear2.bias, 0.)

    def forward(self, x):
        '''
        :param x: (batch * repeat_num for node/edge, emb)
        :return: w and b for affine operation
        '''
        if self.apply_batch_norm:
            x = self.bn_before(x)

        x = self.linear2(self.tanh(self.linear1(x)))
        # x = self.rescale1(x)
        s = x[:, :self.output_dim]
        t = x[:, self.output_dim:]
        s = self.rescale1(torch.tanh(s))
        # s = self.sigmoid(s + self.sigmoid_shift)
        # s = self.rescale2(s) # linear scale seems important, similar to learnable prior..
        return s, t


class ST_Net_Softplus(nn.Module):
    def __init__(self, args, input_dim, output_dim, hid_dim=64, num_layers=2, bias=True, scale_weight_norm=False,
                 sigmoid_shift=2., apply_batch_norm=False):
        super(ST_Net_Softplus, self).__init__()
        self.num_layers = num_layers  # unused
        self.args = args
        self.input_dim = input_dim
        self.hid_dim = hid_dim
        self.output_dim = output_dim
        self.bias = bias
        self.apply_batch_norm = apply_batch_norm
        self.scale_weight_norm = scale_weight_norm
        self.sigmoid_shift = sigmoid_shift

        self.linear1 = nn.Linear(input_dim, hid_dim, bias=bias)
        self.linear2 = nn.Linear(hid_dim, hid_dim, bias=bias)
        self.linear3 = nn.Linear(hid_dim, output_dim * 2, bias=bias)

        if self.apply_batch_norm:
            self.bn_before = nn.BatchNorm1d(input_dim)
        if self.scale_weight_norm:
            self.rescale1 = nn.utils.weight_norm(Rescale_channel(self.output_dim))

        else:
            self.rescale1 = Rescale_channel(self.output_dim)

        self.tanh = nn.Tanh()
        self.softplus = nn.Softplus()
        # self.sigmoid = nn.Sigmoid()
        self.reset_parameters()

    def reset_parameters(self):
        nn.init.xavier_uniform_(self.linear1.weight)
        nn.init.xavier_uniform_(self.linear2.weight)
        nn.init.constant_(self.linear3.weight, 1e-10)
        if self.bias:
            nn.init.constant_(self.linear1.bias, 0.)
            nn.init.constant_(self.linear2.bias, 0.)
            nn.init.constant_(self.linear3.bias, 0.)

    def forward(self, x):
        '''
        :param x: (batch * repeat_num for node/edge, emb)
        :return: w and b for affine operation
        '''
        if self.apply_batch_norm:
            x = self.bn_before(x)

        x = F.tanh(self.linear2(F.relu(self.linear1(x))))
        x = self.linear3(x)
        # x = self.rescale1(x)
        s = x[:, :self.output_dim]
        t = x[:, self.output_dim:]
        s = self.softplus(s)
        s = self.rescale1(s)  # linear scale seems important, similar to learnable prior..
        return s, t


class Rescale(nn.Module):
    def __init__(self):
        super(Rescale, self).__init__()
        self.weight = nn.Parameter(torch.zeros([1]))

    def forward(self, x):
        if torch.isnan(torch.exp(self.weight)).any():
            print(self.weight)
            raise RuntimeError('Rescale factor has NaN entries')

        x = torch.exp(self.weight) * x
        return x


class Rescale_channel(nn.Module):
    def __init__(self, num_channels):
        super(Rescale_channel, self).__init__()
        self.num_channels = num_channels
        self.weight = nn.Parameter(torch.zeros([num_channels]))

    def forward(self, x):
        if torch.isnan(torch.exp(self.weight)).any():
            raise RuntimeError('Rescale factor has NaN entries')

        x = torch.exp(self.weight) * x
        return x


class MaskedGraphAF(nn.Module):
    def __init__(self, mask_node, mask_edge, index_select_edge, num_flow_layer, graph_size,
                 num_node_type, num_edge_type, args, nhid=128, nout=128):
        '''
        :param index_nod_edg:
        :param num_edge_type, virtual type included
        '''
        super(MaskedGraphAF, self).__init__()
        self.repeat_num = mask_node.size(0)
        self.graph_size = graph_size
        self.num_node_type = num_node_type
        self.num_edge_type = num_edge_type
        self.args = args
        self.is_batchNorm = self.args.is_bn

        self.mask_node = nn.Parameter(mask_node.view(1, self.repeat_num, graph_size, 1),
                                      requires_grad=False)  # (1, repeat_num, n, 1)
        self.mask_edge = nn.Parameter(mask_edge.view(1, self.repeat_num, 1, graph_size, graph_size),
                                      requires_grad=False)  # (1, repeat_num, 1, n, n)

        self.index_select_edge = nn.Parameter(index_select_edge, requires_grad=False)  # (edge_step_length, 2)

        self.emb_size = nout
        self.hid_size = nhid
        self.num_flow_layer = num_flow_layer

        self.rgcn = RGCN(num_node_type, nhid=self.hid_size, nout=self.emb_size, edge_dim=self.num_edge_type - 1,
                         num_layers=self.args.gcn_layer, dropout=0., normalization=False)

        if self.is_batchNorm:
            self.batchNorm = nn.BatchNorm1d(nout)

        self.st_net_fn_dict = {'sigmoid': ST_Net_Sigmoid,
                               'exp': ST_Net_Exp,
                               'softplus': ST_Net_Softplus,
                               }
        assert self.args.st_type in ['sigmoid', 'exp',
                                     'softplus'], 'unsupported st_type, choices are [sigmoid, exp, softplus, ]'
        st_net_fn = self.st_net_fn_dict[self.args.st_type]

        self.node_st_net = nn.ModuleList([st_net_fn(self.args, nout, self.num_node_type, hid_dim=nhid, bias=True,
                                                    scale_weight_norm=self.args.scale_weight_norm,
                                                    sigmoid_shift=self.args.sigmoid_shift,
                                                    apply_batch_norm=self.args.is_bn_before) for i in
                                          range(num_flow_layer)])

        self.edge_st_net = nn.ModuleList([st_net_fn(self.args, nout * 3, self.num_edge_type, hid_dim=nhid, bias=True,
                                                    scale_weight_norm=self.args.scale_weight_norm,
                                                    sigmoid_shift=self.args.sigmoid_shift,
                                                    apply_batch_norm=self.args.is_bn_before) for i in
                                          range(num_flow_layer)])

    def forward(self, x, adj, x_deq, adj_deq):
        '''
        :param x:   (batch, N, 9)
        :param adj: (batch, 4, N, N)

        :param x_deq: (batch, N, 9)
        :param adj_deq:  (batch, edge_num, 4)
        :return:
        '''
        # inputs for RelGCNs
        batch_size = x.size(0)
        graph_emb_node, graph_node_emb_edge = self._get_embs(x, adj)

        x_deq = x_deq.view(-1, self.num_node_type)  # (batch *N, 9)
        adj_deq = adj_deq.view(-1, self.num_edge_type)  # (batch*(repeat_num-N), 4)

        for i in range(self.num_flow_layer):
            # update x_deq
            node_s, node_t = self.node_st_net[i](graph_emb_node)
            if self.args.st_type == 'sigmoid':
                x_deq = x_deq * node_s + node_t
            elif self.args.st_type == 'exp':
                node_s = node_s.exp()  # TODO: rescale is exponential, check it.
                x_deq = (x_deq + node_t) * node_s
            elif self.args.st_type == 'softplus':
                x_deq = (x_deq + node_t) * node_s
            else:
                raise ValueError('unsupported st type: (%s)' % self.args.st_type)

            if torch.isnan(x_deq).any():
                raise RuntimeError(
                    'x_deq has NaN entries after transformation at layer %d' % i)

            if i == 0:
                x_log_jacob = (torch.abs(node_s) + 1e-20).log()
            else:
                x_log_jacob += (torch.abs(node_s) + 1e-20).log()

            # update adj_deq
            edge_s, edge_t = self.edge_st_net[i](graph_node_emb_edge)
            if self.args.st_type == 'sigmoid':
                adj_deq = adj_deq * edge_s + edge_t
            elif self.args.st_type == 'exp':
                edge_s = edge_s.exp()
                adj_deq = (adj_deq + edge_t) * edge_s
            elif self.args.st_type == 'softplus':
                adj_deq = (adj_deq + edge_t) * edge_s
            else:
                raise ValueError('unsupported st type: (%s)' % self.args.st_type)

            if torch.isnan(adj_deq).any():
                raise RuntimeError(
                    'adj_deq has NaN entries after transformation at layer %d' % i)
            if i == 0:
                adj_log_jacob = (torch.abs(edge_s) + 1e-20).log()
            else:
                adj_log_jacob += (torch.abs(edge_s) + 1e-20).log()

        x_deq = x_deq.view(batch_size, -1)  # (batch, N * 9)
        adj_deq = adj_deq.view(batch_size, -1)  # (batch, (repeat_num-N) * 4)

        x_log_jacob = x_log_jacob.view(batch_size, -1).sum(-1)  # (batch)
        adj_log_jacob = adj_log_jacob.view(batch_size, -1).sum(-1)  # (batch)
        return [x_deq, adj_deq], [x_log_jacob, adj_log_jacob]

    def reverse(self, x, adj, latent, mode, edge_index=None):
        '''
        Args:
            x: generated subgraph node features so far with shape (1, N, 9), some part of the x is masked
            adj: generated subgraph adacency features so far with shape (1, 4, N, N) some part of the adj is masked
            latent: sample latent vector with shape (1, 9) (mode == 0) or (1, 4) (mode == 1)
            mode: generation mode. if mode == 0, generate a new node, if mode == 1, generate a new edge
            edge_index [1, 2]

        Returns:
            out: generated node/edge features with shape (1, 9) (mode == 0) or (1, 4) , (mode == 1)
        '''

        assert mode == 0 or edge_index is not None, 'if you want to generate edge, you must specify edge_index'
        assert x.size(0) == 1
        assert adj.size(0) == 1
        assert edge_index is None or (edge_index.size(0) == 1 and edge_index.size(1) == 2)

        if mode == 0:  # (1, 9)
            st_net = self.node_st_net
            # emb = graph_emb
            emb = self._get_embs_node(x, adj)

        else:  # mode == 1
            st_net = self.edge_st_net
            emb = self._get_embs_edge(x, adj, edge_index)

        for i in reversed(range(self.num_flow_layer)):
            s, t = st_net[i](emb)
            if self.args.st_type == 'sigmoid':
                latent = (latent - t) / s
            elif self.args.st_type == 'exp':
                s = s.exp()
                latent = (latent / s) - t
            elif self.args.st_type == 'softplus':
                latent = (latent / s) - t

            else:
                raise ValueError('unsupported st type: (%s)' % self.args.st_type)

        return latent

    def _get_embs_node(self, x, adj):
        """
        Args:
            x: current node feature matrix with shape (batch, N, 9)
            adj: current adjacency feature matrix with shape (batch, 4, N, N)
        Returns:
            graph embedding for updating node features with shape (batch, d)
        """

        batch_size = x.size(0)
        adj = adj[:, :3]  # (batch, 3, N, N)

        node_emb = self.rgcn(x, adj)  # (batch, N, d)
        if self.is_batchNorm:
            node_emb = self.batchNorm(node_emb.transpose(1, 2)).transpose(1, 2)  # (batch, N, d)

        graph_emb = torch.sum(node_emb, dim=1, keepdim=False).contiguous()  # (batch, d)
        return graph_emb

    def _get_embs_edge(self, x, adj, index):
        """
        Args:
            x: current node feature matrix with shape (batch, N, 9)
            adj: current adjacency feature matrix with shape (batch, 4, N, N)
            index: link prediction index with shape (batch, 2)
        Returns:
            Embedding(concatenate graph embedding, edge start node embedding and edge end node embedding)
                for updating edge features with shape (batch, 3d)
        """

        batch_size = x.size(0)
        assert batch_size == index.size(0)

        adj = adj[:, :3]  # (batch, 3, N, N)

        node_emb = self.rgcn(x, adj)  # (batch, N, d)
        if self.is_batchNorm:
            node_emb = self.batchNorm(node_emb.transpose(1, 2)).transpose(1, 2)  # (batch, N, d)

        graph_emb = torch.sum(node_emb, dim=1, keepdim=False).contiguous().view(batch_size, 1, -1)  # (batch, 1, d)

        index = index.view(batch_size, -1, 1).repeat(1, 1, self.emb_size)  # (batch, 2, d)
        graph_node_emb = torch.cat((torch.gather(node_emb, dim=1, index=index),
                                    graph_emb), dim=1)  # (batch_size, 3, d)
        graph_node_emb = graph_node_emb.view(batch_size, -1)  # (batch_size, 3d)
        return graph_node_emb

    def _get_embs(self, x, adj):
        '''
        :param x of shape (batch, N, 9)
        :param adj of shape (batch, 4, N, N)
        :return: inputs for st_net_node and st_net_edge
        graph_emb_node of shape (batch*N, d)
        graph_emb_edge of shape (batch*(repeat-N), 3d)

        '''
        # inputs for RelGCNs
        batch_size = x.size(0)
        adj = adj[:, :3]  # (batch, 3, N, N) TODO: check whether we have to use the 4-th slices(virtual bond) or not

        x = torch.where(self.mask_node, x.unsqueeze(1).repeat(1, self.repeat_num, 1, 1), torch.zeros([1]).cuda()).view(
            -1, self.graph_size, self.num_node_type)  # (batch*repeat_num, N, 9)

        adj = torch.where(self.mask_edge, adj.unsqueeze(1).repeat(1, self.repeat_num, 1, 1, 1),
                          torch.zeros([1]).cuda()).view(
            -1, self.num_edge_type - 1, self.graph_size, self.graph_size)  # (batch*repeat_num, 3, N, N)

        node_emb = self.rgcn(x, adj)  # (batch*repeat_num, N, d)

        if self.is_batchNorm:
            node_emb = self.batchNorm(node_emb.transpose(1, 2)).transpose(1, 2)  # (batch*repeat_num, N, d)

        node_emb = node_emb.view(batch_size, self.repeat_num, self.graph_size, -1)  # (batch, repeat_num, N, d)

        graph_emb = torch.sum(node_emb, dim=2, keepdim=False)  # (batch, repeat_num, d)

        #  input for st_net_node
        graph_emb_node = graph_emb[:, :self.graph_size].contiguous()  # (batch, N, d)
        graph_emb_node = graph_emb_node.view(batch_size * self.graph_size, -1)  # (batch*N, d)

        # input for st_net_edge
        graph_emb_edge = graph_emb[:, self.graph_size:].contiguous()  # (batch, repeat_num-N, d)
        graph_emb_edge = graph_emb_edge.unsqueeze(2)  # (batch, repeat_num-N, 1, d)

        all_node_emb_edge = node_emb[:, self.graph_size:]  # (batch, repeat_num-N, N, d)

        index = self.index_select_edge.view(1, -1, 2, 1).repeat(batch_size, 1, 1,
                                                                self.emb_size)  # (batch_size, repeat_num-N, 2, d)

        graph_node_emb_edge = torch.cat((torch.gather(all_node_emb_edge, dim=2, index=index),
                                         graph_emb_edge), dim=2)  # (batch_size, repeat_num-N, 3, d)

        graph_node_emb_edge = graph_node_emb_edge.view(batch_size * (self.repeat_num - self.graph_size),
                                                       -1)  # (batch_size * (repeat_num-N), 3*d)

        return graph_emb_node, graph_node_emb_edge