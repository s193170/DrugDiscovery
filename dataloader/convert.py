
from rdkit import Chem
from rdkit.Chem import AllChem
import torch
import pickle


import numpy as np
import networkx as nx

import torch
from torch.utils.data import Dataset

smiles = pickle.load(open("ZINC_smi.pkl","rb"))
smile = smiles[0]

m = Chem.MolFromSmiles(smile)


# Set graphsize to 39???

#### Make functions for preprocessing - returns X and A
def transform_smile(smile):
    ## Returns X and A from smile
    # Convert molecyle and  kekulize
    m = Chem.MolFromSmiles(smile)
    Chem.Kekulize(m)

    n = m.GetNumAtoms()

    # Make map for atoms
    nums = [6, 7, 8, 9, 15, 16, 17, 35, 53, 0] # C N O F P S Cl Br I
    vals = np.arange(len(nums))
    map_ = dict(zip(nums,vals))


    # Make X
    X = torch.zeros(n,len(nums))
    atoms = [map_[atom.GetAtomicNum()] for atom in m.GetAtoms()]
    X[np.arange(len(atoms)),atoms] = 1

    # Make A
    map_ = {Chem.BondType.SINGLE: 0,Chem.BondType.DOUBLE: 1,Chem.BondType.TRIPLE: 2}
    A = torch.zeros((3, n, n))
    bond_type = [map_[bond.GetBondType()] for bond in m.GetBonds()]
    from_ = [bond.GetBeginAtomIdx() for bond in m.GetBonds()]
    to_ = [bond.GetEndAtomIdx() for bond in m.GetBonds()]

    A[bond_type, from_, to_] = 1
    return X,A

def _save_data(self, path):
        print('saving node/adj feature...')
        print('shape of node feature:', self.node_features.shape)
        print('shape of adj features:', self.adj_features.shape)
        print('shape of mol sizes:', self.mol_sizes.shape)

        torch.save(path + '_node_features', self.node_features)
        torch.save(path + '_adj_features', self.adj_features.astype(np.uint8)) # save space
        torch.save(path + '_mol_sizes', self.mol_sizes) # save space
