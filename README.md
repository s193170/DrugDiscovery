# Drug Discovery

A project on __Generative models for molecular graphs__, supervised by Mikkel N. Schmidt, [mnsc@dtu.dk](mailto:mnsc@dtu.dk).

## Project description

Project description can be found [here](Generative models for molecular graphs.pdf).

## Useful links
### Literature

* Table 2.4 in [this book](https://books.google.dk/books?id=lJ6NAgAAQBAJ&pg=PA46&lpg=PA46&dq=gdb+13+properties&source=bl&ots=QFbCK6eaGn&sig=ACfU3U15hvIF_Vu3pC3BXiBTeQLBae8Phg&hl=da&sa=X&ved=2ahUKEwjp8PaDw_nnAhWElIsKHYSEBQYQ6AEwEXoECAoQAQ#v=onepage&q=gdb%2013%20properties&f=false) is interesting. Also [this resource](https://www.ncbi.nlm.nih.gov/books/NBK6404/).
* [Neural Message Passing for Quantum Chemistry](https://arxiv.org/pdf/1704.01212.pdf)
* [Deep Generative Models for Molecular Science](https://backend.orbit.dtu.dk/ws/portalfiles/portal/145447007/J_rgensen_et_al_2018_Molecular_Informatics.pdf)
* [Learning Deep Generative Models of Graphs](https://openreview.net/pdf?id=Hy1d-ebAb)
* [Variational Graph Auto-Encoders](https://arxiv.org/pdf/1611.07308.pdf)
* [Automatic Chemical Design Using a Data-Driven Continuous Representation of Molecules](https://pubs.acs.org/doi/full/10.1021/acscentsci.7b00572)
* [Moses](https://arxiv.org/pdf/1811.12823.pdf), gives a good overview.
* [How Powerful are Graph Neural Networks?](https://arxiv.org/pdf/1810.00826.pdf) Seems to be a great paper.

### Notes on VAE's
* [Tutorial on Variational Autoencoders](https://arxiv.org/pdf/1606.05908.pdf)
* [Stochastic Backpropagation and Approximate Inference in Deep Generative Models](https://arxiv.org/pdf/1401.4082v3.pdf)
* [Auto-Encoding Variational Bayes](https://arxiv.org/pdf/1312.6114v10.pdf)



## Related projects

* [Efficient learning of non-autoregressive graph variational autoencoders for molecular graph generation](https://doi.org/10.1186/s13321-019-0396-x), [code](https://github.com/seokhokang/graphvae_approx/).
* [Property Prediction with Neural Networks on Raw Molecular Graphs](https://github.com/edvardlindelof/graph-neural-networks-for-drug-discovery).
* [Graph Convolutional Policy Network for Goal-Directed Molecular Graph Generation](https://arxiv.org/pdf/1806.02473.pdf) paper compares JT-VAE, and gives better model. [code](https://github.com/bowenliu16/rl_graph_generation).
* [NeVAE: A Deep Generative Model for Molecular Graphs](https://arxiv.org/abs/1802.05283).
* [Mol-CycleGAN](https://jcheminf.biomedcentral.com/articles/10.1186/s13321-019-0404-1).
* [GraphAF](https://openreview.net/forum?id=S1esMkHYPr). A normalizing flow model.

## Resources

* [GNN Papers](https://github.com/thunlp/GNNPapers).
* [DeepChem](https://deepchem.io/) for TensorFlow. [OpenChem](https://github.com/Mariewelt/OpenChem) seems similar, but less supported.
* [PyTorch Geometric](https://pytorch-geometric.readthedocs.io/en/latest/), for graph neural network modules.
* [MoleculeNet](http://moleculenet.ai/). Benchmarks for models.
* [GuacaMol](https://benevolent.ai/guacamol) and [Moses](https://github.com/molecularsets/moses) give benchmarks for generative models.
* [Papers with code and benchmarks](https://paperswithcode.com/task/drug-discovery).

## Datasets

* [ZINC15](https://zinc.docking.org/).
* [QM9](http://quantum-machine.org/datasets/), a subset of [GDB-13](http://gdb.unibe.ch/downloads/).
* Good resource for datasets is http://moleculenet.ai/datasets-1.

### Misc.
* [Wiki: Drug Design](https://en.wikipedia.org/wiki/Drug_design)
* [Blog: Latent space optimization](https://krasserm.github.io/2018/04/07/latent-space-optimization/)
* [Blog: From Autoencoder to Beta-VAE](https://lilianweng.github.io/lil-log/2018/08/12/from-autoencoder-to-beta-vae.html)
* [Depict smile strings](http://cdb.ics.uci.edu/cgibin/Smi2DepictWeb.py)
* [Slide: Nice overview of important models for molecule generation-](https://jian-tang.com/files/AAAI19/aaai-grltutorial-part3-generation.pdf).
* [Blog: Why polar interpolation / SLERP should be used to interpolate in latent space](https://www.inference.vc/high-dimensional-gaussian-distributions-are-soap-bubble/). A good visual of what is meant/why [here](https://imgur.com/psAfAm5).

### Unrelated.
* [Paper: Variational Autoencoders with Normalizing Flow Decoders (ICLR 2020)](https://openreview.net/forum?id=r1eh30NFwB)

### Questions.
* Why are gradient-free methods usually used to explore the latent space? I.e. Bayesian optimization or reinforcement learning. Why not just gradient descent?
* Normalizing flow vs VAE?

Notes:
When Kekulizing aromatic bonds, we do not really care about which form they're decoded as again. It seems the best method would be to randomly kekulize bonds, and then when calculating the loss, rearomatize the bonds, before matching.