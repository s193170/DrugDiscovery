import pytorch_lightning as pl
import torch
from ReGraphAF.model import MAF
from argparse import ArgumentParser

import torch.nn as nn
import torch.utils.data as data

# TODO: Move to dataset file.
def split_dataset(dataset, val_prop=.1, test_prop=.1, seed=42):
    assert val_prop + test_prop < 1.0

    torch.manual_seed(seed)
    full_size = len(dataset)
    val_size = int(val_prop * full_size)
    test_size = int(test_prop * full_size)
    train_size = full_size - val_size - test_size

    return torch.utils.data.random_split(dataset, [train_size, val_size, test_size])

class ReGraphAF(pl.LightningModule):
    def __init__(self, hparams):
        super().__init__()
        self.hparams = hparams
        if True:
            from dataloader import PretrainZinkDataset
            import pandas as pd
            all_smiles = pd.read_csv(hparams.dataset_path, sep='\n').values
            unique_smiles = frozenset(all_smiles.flatten())
            dataset = PretrainZinkDataset(all_smiles)
            self._train_dataset, self._val_dataset, _ = split_dataset(dataset, seed=args.seed)


        self.model = MAF(in_channels=len(dataset.atoms), num_relations=len(dataset.bonds)-1,
                         hidden_size=hparams.hidden_size, max_graph_nodes=40, max_graph_diameter=12)  # FIXME: get args from dataset.

    def forward(self, x: torch.FloatTensor, adj_type: torch.LongTensor, graph_sizes: torch.LongTensor = None):
        # Maybe do padding.
        return self.model(x, adj_type, graph_sizes)

    def training_step(self, batch, batch_idx):
        x, adj_type, graph_sizes = batch
        negative_log_likelihood = self.model(x, adj_type, graph_sizes)

        loss = negative_log_likelihood.mean(0)  # Take average over the batch.

        logger_logs = {'training_loss': loss}

        # self.show_likelihoods(x[:1], adj_type[:1], graph_sizes[:1])

        return {'loss': loss,
                'log': logger_logs}

    def show_likelihoods(self, x, adj_type, graph_size):
        assert x.shape[0] == 1
        train_state = self.model.training
        self.model.eval()
        (node_eps, edge_eps), (node_likelihood, edge_likelihood) = self.model(x, adj_type, graph_size, verbose=True)

        graph_size = x.shape[-2] # graph_size.item()
        matrix = adj_type.new_zeros((graph_size, graph_size), dtype=torch.float)
        # self.model.
        for i in range(graph_size):
            matrix[i, i] = node_likelihood[0, i]
        #for xy in self.model.link_prediction_index[i]:
            #matrix[xy] = node_likelihood[xy, i]

        self.model.train(train_state)  # Restore state.


    def configure_optimizers(self):
        return torch.optim.Adam(list(filter(lambda p: p.requires_grad, self.model.parameters())), lr=self.hparams.lr)

    def train_dataloader(self):
        return data.DataLoader(self._train_dataset, batch_size=args.batch_size,
                               num_workers=args.num_workers, shuffle=True, pin_memory=True)

    #def val_dataloader(self):
    #    return data.DataLoader(self._val_dataset, batch_size=args.batch_size,
    #                           num_workers=args.num_workers, shuffle=False, pin_memory=True)  # No reason to shuffle.







if __name__ == "__main__":
    parser = ArgumentParser()
    parser = pl.Trainer.add_argparse_args(parser)  # Add trainer options.

    # General training parameters
    parser.add_argument('--seed', default=42, type=int, help='Set the seed for the RNG.')
    parser.add_argument('--num_workers', default=0, type=int, help='Number of workers in dataloader.')
    parser.add_argument('--log_level', default=0, type=int,
                        help='The level of debugging logs to save (mainly tensorboard logs)')
    parser.add_argument('--dataset_path', default='ReGraphAF/dataset/250k_rndm_zinc_drugs_clean_sorted.smi')

    # Hyperparameters
    parser.add_argument('--batch_size', default=32, type=int)
    parser.add_argument('--hidden_size', default=128, type=int)
    parser.add_argument('--length_normalize', default=False, type=bool,
                        help='If the loss should be normalized with respect to the size of the graph.')  # FIXME: Impement this.
    parser.add_argument('--lr', default=1e-3, type=float)


    args = parser.parse_args()
    model = ReGraphAF(args)

    trainer = pl.Trainer.from_argparse_args(args)
    trainer.fit(model)