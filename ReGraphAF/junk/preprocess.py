


import sys
import os
import argparse
import numpy as np
from rdkit import Chem
from rdkit.Chem import rdmolops
import torch_sparse
import torch
import torch.nn.functional as F
import torch.nn as nn
import pandas as pd
import pickle

from tqdm import tqdm

def convert_smile_ZINC(smile='c1ccccc1'):

    m = Chem.MolFromSmiles(smile)
    Chem.Kekulize(m)

    adjmat = torch.as_tensor(Chem.GetAdjacencyMatrix(m,useBO=True), dtype=torch.long)
    adjmat = torch_sparse.SparseTensor.from_dense(adjmat)

    #### Make node matrix
    atoms = ['C','N','O','F','P','S','Cl','Br','I']
    atoms = {C:i for i, C in enumerate(atoms)}
    nodes = torch.as_tensor([atoms[atom.GetSymbol()] for atom in m.GetAtoms()], dtype=torch.long )
    mol_size = len(nodes)
    x = nn.functional.one_hot(nodes, num_classes=len(atoms)).float()

    return x,adjmat,mol_size



def convert_all(source,destination,converter):
    '''
    Converts all smile strings in source and saves adjacency matrix, molecular sizes and nodes in destination
    :param source:
    :param destination:
    :return:
    '''

    cnt = 0
    nodes = []
    adjs = []
    mol_sizes = []
    fp = open(source, 'r')
    for smiles in tqdm(fp):
        smiles = smiles.strip()
        node,adjmat,mol_size = converter(smiles)

        nodes.append(node)
        adjs.append(adjmat)
        mol_sizes.append(mol_size)


    # Save nodes, edges and sizes
    f = open(destination + '_nodes.pkl', "wb")
    pickle.dump(nodes, f)
    f.close()

    f = open(destination + '_adj.pkl', "wb")
    pickle.dump(adjs, f)
    f.close()

    f = open(destination + '_mol_sizes.pkl', "wb")
    pickle.dump(mol_sizes, f)
    f.close()

if __name__ == '__main__':
    # 'dataset/ZINC_smi.pkl'
    # 'data_processed'

    data_set = sys.argv[1]

    if data_set == 'ZINC':
        source = 'dataset/250k_rndm_zinc_drugs_clean_sorted.smi'
        destination = 'data_processed/'
        convert_all(source, destination, convert_smile_ZINC)