

from time import time
import argparse
import numpy as np
import math
import os
import sys
import json
from tqdm import tqdm

import pickle
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
from torch.utils.data import DataLoader
from rdkit import Chem
from model import *
from dataloader import *
import torch_sparse
from torch.utils.data import Dataset



def restore_model(model, epoch = None):
    if epoch is None:
        restore_path = os.path.join('../saved_models', 'checkpoint')
        print('Restore from the latest checkpoint')
    else:
        restore_path = os.path.join('../saved_models', 'checkpoint%s' % str(epoch))
        print('restore from checkpoint%s' % str(epoch))

    if torch.cuda.is_available():
        checkpoint = torch.load(restore_path)
    else:
        checkpoint = torch.load(restore_path,map_location=torch.device('cpu'))
    state_dict = checkpoint['model_state_dict']
    model.load_state_dict(checkpoint['model_state_dict'])


model = MAF()
restore_model(model)
