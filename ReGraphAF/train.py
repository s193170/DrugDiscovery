

from time import time
import argparse
import numpy as np
import math
import os
import sys
import json
from tqdm import tqdm
import pickle
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
from torch.utils.data import DataLoader, Subset
from rdkit import Chem
from model import *

from rdkit.Chem.Descriptors import qed, MolLogP
from dataloader import *
import torch_sparse
from torch.utils.data import Dataset
import pandas as pd
from sascorer import calculateScore

#from model_old import *

def get_QED(smiles):
    print("Getting QED")
    QEDs = np.zeros(len(smiles))
    for i in tqdm(range(len(smiles))):
        smile = smiles[i]
        m = Chem.MolFromSmiles(smile)
        QEDs[i] = QED.qed(m,w=QED.properties(m))
    return QEDs


def penalized_logp(mol):
    """
    Reward that consists of log p penalized by SA and # long cycles,
    as described in (Kusner et al. 2017). Scores are normalized based on the
    statistics of 250k_rndm_zinc_drugs_clean.smi dataset
    :param mol: rdkit mol object
    :return: float
    """
    # normalization constants, statistics from 250k_rndm_zinc_drugs_clean.smi
    logP_mean = 2.4570953396190123
    logP_std = 1.434324401111988
    SA_mean = -3.0525811293166134
    SA_std = 0.8335207024513095
    cycle_mean = -0.0485696876403053
    cycle_std = 0.2860212110245455

    log_p = MolLogP(mol)
    SA = -calculateScore(mol)

    # cycle score
    cycle_list = nx.cycle_basis(nx.Graph(
        Chem.rdmolops.GetAdjacencyMatrix(mol)))
    if len(cycle_list) == 0:
        cycle_length = 0
    else:
        cycle_length = max([len(j) for j in cycle_list])
    if cycle_length <= 6:
        cycle_length = 0
    else:
        cycle_length = cycle_length - 6
    cycle_score = -cycle_length

    normalized_log_p = (log_p - logP_mean) / logP_std
    normalized_SA = (SA - SA_mean) / SA_std
    normalized_cycle = (cycle_score - cycle_mean) / cycle_std

    return normalized_log_p + normalized_SA + normalized_cycle


def split_dataset(dataset, val_prop=.1, test_prop=.1, random_seed=42):
    assert val_prop + test_prop < 1.0

    torch.manual_seed(random_seed)

    full_size = len(dataset)
    val_size = int(val_prop * full_size)
    test_size = int(test_prop * full_size)
    train_size = full_size - val_size - test_size

    return torch.utils.data.random_split(dataset, [train_size, val_size, test_size])


def read_molecules(path):
    print('reading data from %s' % path)
    node_features = np.load(path + '_node_features.npy')
    adj_features = np.load(path + '_adj_features.npy')
    mol_sizes = np.load(path + '_mol_sizes.npy')

    fp = open(path + '_raw_smiles.smi')
    all_smiles = []
    for smiles in fp:
        all_smiles.append(smiles.strip())
    fp.close()
    return node_features, adj_features, mol_sizes, all_smiles


def save_model(model, optimizer, args, var_list, epoch=None):
    epoch = str(epoch) if epoch is not None else ''
    latest_save_path = os.path.join(args['save_path'], 'checkpoint')
    final_save_path = os.path.join(args['save_path'], 'checkpoint%s' % epoch)
    torch.save({
        **var_list,
        'model_state_dict': model.state_dict(),
        'optimizer_state_dict': optimizer.state_dict()},
        final_save_path
    )

    # save twice to maintain a latest checkpoint
    torch.save({
        **var_list,
        'model_state_dict': model.state_dict(),
        'optimizer_state_dict': optimizer.state_dict()},
        latest_save_path
    )


def restore_model(model, args, epoch = None):
    if epoch is None:
        restore_path = os.path.join(args['save_path'], 'checkpoint')
        print('Restore from the latest checkpoint')
    else:
        restore_path = os.path.join(args['save_path'], 'checkpoint%s' % str(epoch))
        print('restore from checkpoint%s' % str(epoch))

    checkpoint = torch.load(restore_path)
    model.load_state_dict(checkpoint['model_state_dict'])

class Trainer(object):
    def __init__(self, dataloader, unique_smiles, args, train_smiles = None):
        self.dataloader = dataloader
        self.args = args

        self.all_train_smiles = unique_smiles

        # Unwrap args
        self.cuda =  self.args['cuda']
        self.lr = args['lr']
        self.weight_decay = args['weight_decay']
        self.epochs = args['epochs']
        self.save = args['save']

        # Model and optimizer
        self._model = MAF()
        # Check GPU
        if self.cuda:
            self._model =  self._model.cuda()

        self._optimizer = optim.Adam(filter(lambda p: p.requires_grad, self._model.parameters()),
                                     lr=self.lr, weight_decay=self.weight_decay)

        self.best_loss = 100.0
        self.start_epoch = 0



    def generate_molecule(self, num=100, epoch = None, out_path = None, mute = False, save_good_mol = False):
        self._model.eval()

        all_smiles = []
        pure_valids = []
        appear_in_train = 0.
        start_t = time()
        cnt_mol = 0
        cnt_gen = 0

        while cnt_mol < num:
            smiles, no_resample, num_atoms = self._model.sample() # Drop arguments for now
            cnt_gen += 1
            print(cnt_mol, cnt_gen, smiles)
            if num_atoms < 5:
                print(f"#atoms of generated molecule less than {5}")
                print(cnt_mol, cnt_gen)
                #if cnt_gen > 10:
                #    exit()
            else:
                cnt_mol += 1

                if cnt_mol % 100 == 0:
                    print('cur cnt mol: %d' % cnt_mol)

                all_smiles.append(smiles)
                pure_valids.append(no_resample)

                if  self.all_train_smiles is not None and smiles in self.all_train_smiles:
                    appear_in_train += 1.0

            mol = Chem.MolFromSmiles(smiles)
            qed_score = qed(mol)
            plogp_score = penalized_logp(mol)

        assert cnt_mol == num, 'number of generated molecules does not equal num'

        unique_smiles  = set(all_smiles)
        unique_rate =  len(unique_smiles) / num
        pure_valid_rate = sum(pure_valids)/num

        novelty =  1. - (appear_in_train / num)

        if epoch is None:
            print('Time of generating (%d/%d) molecules(#atoms>=%d): %.5f | unique rate: %.5f | valid rate: %.5f | novelty: %.5f' % (num,
                                cnt_gen, 5, time()-start_t, unique_rate, pure_valid_rate, novelty))
        else:
            print('Time of generating (%d/%d) molecules(#atoms>=%d): %.5f at epoch :%d | unique rate: %.5f | valid rate: %.5f | novelty: %.5f' % (num,
                                cnt_gen, 5, time()-start_t, epoch, unique_rate, pure_valid_rate, novelty))


        if out_path is not None and self.save:
            fp = open(out_path, 'w')
            cnt = 0
            for i in range(len(all_smiles)):
                fp.write(all_smiles[i] + '\n')
                cnt +=  1
            fp.close()
            print('writing %d smiles into %s done' % (cnt, out_path))

        return (unique_rate, pure_valid_rate, novelty)

    def fit(self, mol_out_dir):

        t_total = time()
        total_loss = []
        best_loss = self.best_loss
        start_epoch = self.start_epoch

        all_unique_rate = []
        all_valid_rate = []
        all_novelty_rate = []

        for epoch in range(self.epochs):
            epoch_loss = self.train_epoch(epoch)
            total_loss.append(epoch_loss)

            if epoch_loss < best_loss:
                best_loss = epoch

            if self.save:
                var_list = {'cur_epoch': epoch + start_epoch,
                            'best_loss': best_loss,
                            }
                save_model(self._model, self._optimizer, self.args, var_list, epoch=epoch + start_epoch)

            if mol_out_dir is not None and self.args['generate_molecules']:
                mol_save_path = os.path.join(mol_out_dir, f'epoch{epoch + start_epoch}.txt') if mol_out_dir is not None else None
                cur_unique, cur_valid, cur_novelty = self.generate_molecule(num=100, epoch = epoch + start_epoch, out_path=mol_save_path, mute= True)
                all_novelty_rate.append(cur_novelty)
                all_unique_rate.append(cur_unique)
                all_valid_rate.append(cur_valid)


        print("Optimization Finished!")
        print(f"Total time elapsed: {time() - t_total}")

        if mol_out_dir is not None and self.args['generate_molecules']:
            all_unique_rate = np.array(all_unique_rate)
            all_valid_rate = np.array(all_valid_rate)
            all_novelty_rate = np.array(all_novelty_rate)
            print('saving unique and valid array...')
            np.save(os.path.join(mol_out_dir, 'unique'), all_unique_rate)
            np.save(os.path.join(mol_out_dir, 'valid'), all_valid_rate)
            np.save(os.path.join(mol_out_dir, 'novelty'), all_novelty_rate)


    def train_epoch(self, epoch_cnt):
        t_start = time()
        batch_losses = []
        batch_cnt = 0
        epoch_example = 0
        self._model.train()
        for i_batch, batch_data in enumerate(tqdm(self.dataloader)):
            batch_time_s = time()
            self._optimizer.zero_grad()
            batch_cnt += 1
            inp_node_features, inp_adj_features, inp_mol_size = batch_data

            if self.cuda:
                inp_node_features = inp_node_features.cuda()
                inp_adj_features = inp_adj_features.cuda()
                inp_mol_size = inp_mol_size.cuda()
            loss = self._model(inp_node_features, inp_adj_features, inp_mol_size)
            loss.mean().backward()
            self._optimizer.step()
            batch_losses.append(loss.mean())  # FIXME: This is a memory leak. (No detach())

        epoch_loss = sum(batch_losses) / (len(batch_losses))
        print('Epoch: {: d}, loss {:5.5f}, epoch time {:.5f}'.format(epoch_cnt, epoch_loss, time()-t_start))
        return epoch_loss

if __name__ == "__main__":
    #### Device check
    print(f'GPU: {torch.cuda.is_available()}')

    ### Parameters
    args = {}
    args['batch_size'] = 32
    args['num_workers'] = 0
    args['shuffle'] = True
    args['num_workers'] = 0
    args['cuda'] = torch.cuda.is_available()
    args['lr'] = 0.001
    args['weight_decay'] = 0.0
    args['save'] = True
    args['epochs'] = 10
    args['save_path'] = 'saved_models'
    args['generate_molecules'] = True


    source = 'dataset/250k_rndm_zinc_drugs_clean_sorted.smi'  # Fixme: Stereochemisty

    all_smiles = pd.read_csv(source, sep='\n').values

    unique_smiles = frozenset(all_smiles.flatten())

    train_data, test_data, _ = split_dataset(PretrainZinkDataset(all_smiles))

    print(len(train_data))
    #train_dataloader = DataLoader(Subset(train_data, range(32*500)),shuffle=args['shuffle'],num_workers=args['num_workers'],batch_size=args['batch_size'])
    train_dataloader = DataLoader(Subset(train_data, range(args['batch_size']*1000)),shuffle=args['shuffle'],num_workers=args['num_workers'],batch_size=args['batch_size'])

    len(train_data)

    trainer = Trainer(train_dataloader, unique_smiles, args)
    trainer.fit(args['save_path'])