import numpy as np
import networkx as nx

import torch
import torch_sparse
import pickle
from torch.utils.data import Dataset
from rdkit import Chem
import torch.nn
import torch.nn as nn
import pandas as pd



def bfs_seq(G, start_id):
    '''
    get a bfs node sequence
    :param G:
    :param start_id:
    :return:
    '''
    dictionary = dict(nx.bfs_successors(G, start_id))
    start = [start_id]
    output = [start_id]
    while len(start) > 0:
        next = []
        while len(start) > 0:
            current = start.pop(0)
            neighbor = dictionary.get(current)
            if neighbor is not None:
                next = next + neighbor
        output = output + next
        start = next
    return output

import rdkit.Chem.rdchem as rdchem
class PretrainZinkDataset(Dataset):
    def __init__(self, smiles):
        self.n_molecule = len(smiles)
        self.smiles = smiles

        peridoc_table = rdchem.GetPeriodicTable()
        atoms = ('C', 'N', 'O', 'F', 'P', 'S', 'Cl', 'Br', 'I')
        bonds = (None, rdchem.BondType.SINGLE, rdchem.BondType.DOUBLE, rdchem.BondType.TRIPLE)
        self.atoms = {atom:idx for idx, atom in enumerate(atoms)}
        self.bonds = {bond:idx for idx, bond in enumerate(bonds)}
        self.max_size = 40 # Set to this due to padding issues

    def __len__(self):
        return self.n_molecule

    def convert_smile_ZINC(self, smile):
        m = Chem.MolFromSmiles(smile)
        Chem.Kekulize(m)

        adjmat = torch.as_tensor(Chem.GetAdjacencyMatrix(m, useBO=True), dtype=torch.long)

        #### Make node matrix
        # atoms = {C: i for i, C in enumerate(atoms)}
        nodes = torch.as_tensor([self.atoms[atom.GetSymbol()] for atom in m.GetAtoms()], dtype=torch.long)
        mol_size = len(nodes)
        x = nn.functional.one_hot(nodes, num_classes=len(self.atoms)).float()

        return x, adjmat, mol_size

    def __getitem__(self, idx):
        ### Get order of items and performs BFS re-ordering

        smile = self.smiles[idx][0]
        node_features, adj_features, mol_size = self.convert_smile_ZINC(smile)

        local_perm = np.random.permutation(mol_size)  # (first perm graph)
        adj_perm = adj_features[np.ix_(local_perm, local_perm)]
        adj_perm_matrix = np.asmatrix(adj_perm)
        G = nx.from_numpy_matrix(adj_perm_matrix)
        start_idx = np.random.randint(adj_perm.shape[0])  # operated on permed graph
        bfs_perm = np.array(bfs_seq(G, start_idx))  # get a bfs order of permed graph
        bfs_perm_origin = local_perm[bfs_perm]

        # local perm, bfs perm, bfs_perm_origin
        # Id -> Local Perm
        # Local Perm -> BFS Ordering of Local Perm
        # local_perm @ bfs_perm = Id -> Local Perm -> BFS Ordering of Local Perm

        adj_perm = torch.zeros((self.max_size, self.max_size), dtype=adj_features.dtype)
        adj_perm[:mol_size, :mol_size] = adj_features[np.ix_(bfs_perm_origin, bfs_perm_origin)]
        # A = torch_sparse.SparseTensor.from_dense(adj_perm) wait with this for now
        A = adj_perm
        X = torch.zeros((self.max_size, len(self.atoms)))

        X[:mol_size,:] = node_features[bfs_perm_origin,:]


        return X, A, torch.tensor(mol_size)


# Useless

"""
class DataIterator(object):
    def __init__(self, dataloader):
        self.iterator = self.one_shot_iterator(dataloader)

    def __next__(self):
        data = next(self.iterator)
        return data

    @staticmethod
    def one_shot_iterator(dataloader):
        '''
        #Transform a PyTorch Dataloader into python iterator
        '''
        while True:
            for data in dataloader:
                yield data
"""

if __name__ == "__main__":

    f = open("data_processed/_adj.pkl", "rb")
    adj = pickle.load(f)
    f.close()

    f = open("data_processed/_nodes.pkl", "rb")
    nodes = pickle.load(f)
    f.close()

    f = open("data_processed/_mol_sizes.pkl", "rb")
    mol_sizes = pickle.load(f)
    f.close()

    # Quick test - looks  fine
    a = PretrainZinkDataset(nodes,adj,mol_sizes)

    A= a.__getitem__(1)