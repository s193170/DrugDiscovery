
import pandas as pd


import os.path

import numpy as np
import networkx as nx

import torch
import torch_sparse
import pickle
from torch.utils.data import Dataset
from rdkit import Chem
import torch.nn
import torch.nn as nn
import os
import h5py

from os import path

from tqdm import tqdm

class MolDataLoader(Dataset):
    def __init__(self, source):

        ### Extract name and check if hdf5 exists
        name,type = source.split('.')
        name = source.split('.')[0] + '.hdf5'
        exists = path.exists(name)

        if exists:
            ### Load file and save stuff
            self.f = h5py.File(name, 'r')

            self.smiles = self.f['smiles']
            self.all_smiles = set(self.smiles[()])
            # Save attributes
            attrs = self.smiles.attrs
            self.max_atoms = attrs['max_atoms']
            self.max_size = self.max_atoms + 2 # Padding

            self.nmol = attrs['nmol']
            self.diameter = attrs['diameter']
            atoms = attrs['atoms'].split(',')
            atoms.pop()
            self.atoms = atoms
            #f.close() <- don't  close


        # Perform preprocessing
        else:
            print('Creating h5 dataset since no h5 file was found')
            self.smiles = pd.read_csv(source, sep='\n',header=None)[0].values # Pandas seems to be better for loading (less lag)
            self.nmol = len(self.smiles)
            f = h5py.File(name, 'w')
            dt = h5py.string_dtype()
            dset = f.create_dataset('smiles',(self.nmol,),dtype=dt)
            all_smiles, nmol,atoms, max_atoms, diameter = self.process(dset)
            attr = dset.attrs
            attr['nmol'] = nmol
            attr['atoms'] = atoms
            attr['max_atoms'] = max_atoms
            attr['diameter'] = diameter

            f.close()

    def __len__(self):
        return self.nmol

    def process(self,dset):
        all_smiles = set()
        max_atoms = 0
        atoms = set()
        diameter = 12 #Just want to get basic things working so we are setting this manually for now

        for i, smile in enumerate(tqdm(self.smiles)):
            m = Chem.MolFromSmiles(smile)
            ### Convert to the form we want
            Chem.Kekulize(m)
            Chem.RemoveStereochemistry(m)
            Chem.RemoveHs(m)
            Chem.SanitizeMol(m)

            ### Find unique atoms
            ATOMS = m.GetAtoms()

            for atom in ATOMS:
                atoms.add(atom.GetSymbol())

            # Check if it is larger
            max_atoms = len(ATOMS) if max_atoms < len(ATOMS) else max_atoms

            # Convert back
            dt = h5py.string_dtype()
            s = Chem.MolToSmiles(m)


            dset[i] = s
            all_smiles.add(m)

        ### Convert atoms to desired format
        foo = ''
        for atom in atoms:
            foo += atom + ','
        atoms = foo

        nmols = len(all_smiles)

        return all_smiles, nmols, atoms, max_atoms, diameter

    def convert_smile_ZINC(self, smile):
        m = Chem.MolFromSmiles(smile)
        Chem.Kekulize(m)
        atoms = self.atoms

        adjmat = torch.as_tensor(Chem.GetAdjacencyMatrix(m, useBO=True), dtype=torch.long)

        #### Make node matrix
        #atoms = ['C', 'N', 'O', 'F', 'P', 'S', 'Cl', 'Br', 'I'] <---- for Zink dataset
        atoms = {C: i for i, C in enumerate(self.atoms)}
        nodes = torch.as_tensor([atoms[atom.GetSymbol()] for atom in m.GetAtoms()], dtype=torch.long)

        mol_size = len(nodes)
        x = nn.functional.one_hot(nodes, num_classes=len(atoms)).float()

        return x, adjmat, mol_size

    def __getitem__(self, idx):
        ### Get order of items and performs BFS re-ordering

        smile = self.smiles[idx]
        node_features, adj_features, mol_size = self.convert_smile_ZINC(smile)

        local_perm = np.random.permutation(mol_size)  # (first perm graph)
        adj_perm = adj_features[np.ix_(local_perm, local_perm)]
        adj_perm_matrix = np.asmatrix(adj_perm)
        G = nx.from_numpy_matrix(adj_perm_matrix)
        start_idx = np.random.randint(adj_perm.shape[0])  # operated on permed graph
        bfs_perm = np.array(self.bfs_seq(G, start_idx))  # get a bfs order of permed graph
        bfs_perm_origin = local_perm[bfs_perm]

        # local perm, bfs perm, bfs_perm_origin
        # Id -> Local Perm
        # Local Perm -> BFS Ordering of Local Perm
        # local_perm @ bfs_perm = Id -> Local Perm -> BFS Ordering of Local Perm

        adj_perm = torch.zeros((self.max_size, self.max_size))
        adj_perm[:mol_size, :mol_size] = adj_features[np.ix_(bfs_perm_origin, bfs_perm_origin)]
        # A = torch_sparse.SparseTensor.from_dense(adj_perm) wait with this for now
        A = adj_perm
        X = torch.zeros((self.max_size,len(self.atoms)))

        X[:mol_size,:] = node_features[bfs_perm_origin,:]

        return {'node': X.float(), 'adj': A, 'mol_size': mol_size}

    def bfs_seq(self,G, start_id):
        '''
        get a bfs node sequence
        :param G:
        :param start_id:
        :return:
        '''
        dictionary = dict(nx.bfs_successors(G, start_id))
        start = [start_id]
        output = [start_id]
        while len(start) > 0:
            next = []
            while len(start) > 0:
                current = start.pop(0)
                neighbor = dictionary.get(current)
                if neighbor is not None:
                    next = next + neighbor
            output = output + next
            start = next
        return output


if __name__ == "__main__":
    source = 'dataset/250k_rndm_zinc_drugs_clean_sorted.smi'
    a = MolDataLoader(source)