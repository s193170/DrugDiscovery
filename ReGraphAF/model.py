import torch
import torch.nn as nn
import torch_geometric as geom
from torch_scatter import scatter_add
from torch_geometric.utils import add_remaining_self_loops
import torch.nn.init as init
import math
import torch.distributions
import torch.nn.functional as F
import numpy as np
import copy

from rdkit import Chem

# In the paper, an edge-conditioned adjecency tensor is used at every message passing step.
#  It should only really be applied on the first.

# Weakness 1:
#  Since the GCNConv is permutation-invariant, there is no way to represent or learn Stereoisomerism.
#   This could be fixed by letting a separate network learn this on the final molecule. But it shows that one should not
#    expect to e.g. regress atomic positions well from a basic GNN. Note how a SMILES-representation lack this weakness.


class RGCNConv(geom.nn.MessagePassing):
    # A mix of GCNConv and RGCNConv to allow for the spectral normalization.
    def __init__(self, in_channels, out_channels, num_relations, bias=True, normalize=True, **kwargs):
        super().__init__(aggr='add' if normalize else 'mean', **kwargs)

        self.normalize = normalize
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.num_relations = num_relations

        # We can do the basis decomposition in case there's overfitting even in one layer?
        #self.basis = nn.Param(torch.Tensor(num_bases, in_channels, out_channels))
        #self.att = nn.Param(torch.Tensor(num_relations, num_bases))
        self.w = nn.Parameter(torch.Tensor(num_relations, in_channels , out_channels))

        self.root = nn.Parameter(torch.Tensor(in_channels, out_channels))

        if bias:
            self.bias = nn.Parameter(torch.Tensor(out_channels))
        else:
            self.register_parameter('bias', None)

        self.reset_parameters()

    def reset_parameters(self):
        init.kaiming_uniform_(self.w, a=math.sqrt(5)) # nonlinearity='relu', nonlinearity='conv1d'?
        init.kaiming_uniform_(self.root, a=math.sqrt(5)) # nn.init.xavier_uniform_?
        if self.bias is not None:
            # init.zeros_(self.bias)
            fan_in, _ = init._calculate_fan_in_and_fan_out(self.root)
            bound = 1 / math.sqrt(fan_in)
            init.uniform_(self.bias, -bound, bound)

    @staticmethod
    def norm(edge_index, num_nodes, edge_weight=None):
        row, col = edge_index
        deg = scatter_add(edge_weight, row, dim=0, dim_size=num_nodes)
        deg_inv_sqrt = deg.pow(-0.5)
        deg_inv_sqrt[deg_inv_sqrt == float('inf')] = 0
        return deg_inv_sqrt[row] * edge_weight * deg_inv_sqrt[col]

    def message(self, x_j, edge_index_j, edge_type, edge_weight):
        # This will create a w for each node. It is probably much better performance wise to multiply (each) w with x in
        #  the forward step, and then select the right feature based on edge type in the messaging step!
        #  I.e. the difference is only the order of operations. (Since we have few relations, the other way is faster.)
        w = torch.index_select(self.w, 0, edge_type)
        out = torch.bmm(x_j.unsqueeze(1), w).squeeze(-2)
        return out if edge_weight is None else out * edge_weight.view(-1, 1)

    def update(self, aggr_out, x):
        aggr_out = aggr_out + torch.matmul(x, self.root)

        if self.bias is not None:
            aggr_out = aggr_out + self.bias

        return aggr_out

    def forward(self, x, edge_index, edge_type, edge_weight=None, size=None):
        if self.normalize:
            if edge_weight is None:
                edge_weight = torch.ones((edge_index.size(1),), dtype=x.dtype, device=edge_index.device)
            edge_weight = self.norm(edge_index, x.size(self.node_dim), edge_weight)
        return self.propagate(edge_index, size=size, x=x, edge_type=edge_type, edge_weight=edge_weight)

class GraphAFConv(nn.Module):
    def __init__(self, in_channels, out_channels, num_relations, bias=True,
                 improved=False):
        super().__init__()
        self.convs = nn.ModuleList([geom.nn.DenseGCNConv(in_channels, out_channels, improved=improved,
                                                         bias=bias and i==0) for i in range(num_relations)])

    def forward(self, x, adj_feat):
        return torch.stack([self.convs[i](x, adj_feat[..., i, :, :], add_loop=True) for i in range(len(self.convs))],
                           dim=0).sum(dim=0)


class RGCN(nn.Module):
    def __init__(self, in_channels, out_channels=128, num_relations=3, batchnorm=True):
        super().__init__()
        # https://math.stackexchange.com/questions/3035968/interpretation-of-symmetric-normalised-graph-adjacency-matrix
        # We should also try a mean aggregation. It is faster to compute, and node degrees do not vary significantly so
        # symmetric normalization probably does not give much benefit. Additionally, I think symmetric normalization
        # intuitively works well for learning graph structure features (since both neighbours outgoing connections
        # as well as one's own are taken into account) But mean normalization will probably work when it comes to e.g.
        # identifying functional groups, since it doesn't have "noise" from the values of who the neighbours,
        # and neighbours neighbours are connected to.

        # An example is with the global sum aggregation. If we have mean normalization, then this can give the number of atoms of each type in the graph.
        # But this also exposes a weakness: that it has less immediate structure knowledge. (but again, a few layers of graph conv and this problem is solved)

        if batchnorm:
            self.bn = nn.BatchNorm1d(out_channels, affine=True)
        else:
            self.register_parameter('bn', None)

        if True:
            # The network, as presented in the GraphAF code.
            self.layers = nn.ModuleList([nn.Linear(in_channels, in_channels, bias=False), # Why linear before linear?
                                        GraphAFConv(in_channels, 128, num_relations, bias=False), nn.ReLU(),
                                        GraphAFConv(128, 128, num_relations, bias=False), nn.ReLU(),
                                        GraphAFConv(128, out_channels, num_relations, bias=False)])
        else:
            # Interesting paper about the power of graph conv types: https://arxiv.org/pdf/1810.00826.pdf
            #  I am so confused. How was this paper released after Kipfs with symmetric normalization?: https://arxiv.org/pdf/1810.02244.pdf, GraphConv seems like a much more natural extension.
            # It argues GCN is good for node classification, since it uses mean aggregation, but this reduces the graph
            #  classification performance. I.e. Imagine a graph only consisting of carbon atoms and single bonds, there
            # is literally no way for GCN to distinguish C-C, C-C-C, ..., since these atoms all have the same initial embedding!
            #  All nodes have the same values. Unless we perform symmetric normalization (as opposed to mean normalization)
            # With symmetic normalization the problem is alleviated a bit, but it will probably be hard to distinguish
            #  well e.g. C-C-C-C-C=C5H12=pentane and C-C-C-C-C-C=C6H13=hexane, which may or may not be what you want.

            # How about cycles? Will the GNN's be able to distinguish if there is a cycle? I mean there's no difference
            #  between receiving own-features twice, or features from someone with the same initial features, and similar neighbours.


            # Wow how did i not think about combining dilated convolutions? (res-net is also a good idea): https://arxiv.org/pdf/1904.03751.pdf
            # First Conv with a distance of 1, A, then a distance of 2, A^2==1, then (A^2)^2==1 and so on...
            # One could even do pooling based on this information

            # GRAPH NEURAL NETWORKS EXPONENTIALLY LOSE EXPRESSIVE POWER FOR NODE CLASSIFICATION: https://openreview.net/pdf?id=S1ldO2EFPr

            # The greatest eigenvalue of the adjacency matrix is bounded by the maximum degree. So it will probably be 4.

            # We want to try GINConv, GraphConv, GCNConv. When we find the best, try with dilation and residual connections.

            # Paper: Isomorphism of graphs of bounded valence can be tested in polynomial time. (Maybe we can generalize?) https://en.wikipedia.org/wiki/Graph_isomorphism_problem

            # The difference is transductive/spectral/symmetric vs inductive/spatial/mean.
            self.layers = nn.ModuleList([RGCNConv(in_channels, 64, num_relations), nn.ReLU(),
                                         geom.nn.GCNConv(64, 64), nn.ReLU(),
                                         geom.nn.GCNConv(64, out_channels), nn.ReLU(),
                                         RGCNConv(out_channels, out_channels, bias=not batchnorm)])

    def forward(self, x, adj_feat):
        # adj = torch.any(adj_feat != 0, dim=1)  # Adjacency matrix.

        for layer in self.layers:
            if isinstance(layer, GraphAFConv):
                x = layer(x, adj_feat)
            else:
                x = layer(x)

        if self.bn is not None:
            # Note that the batch norm expect data to lie in channels (batch, d, N), so we must transpose.
            x = self.bn(x.transpose(1, 2)).transpose(1, 2)

        return x


class MAF(nn.Module):

    @staticmethod
    def initialize_masks(max_node_unroll=38, max_edge_unroll=12):
        """
        Args:
            max node unroll: maximal number of nodes in molecules to be generated (default: 38)
            max edge unroll: maximal number of edges to predict for each generated nodes (default: 12, calculated from zink250K data)
        Returns:
            node_masks: node mask for each step
            adj_masks: adjacency mask for each step
            is_node_update_mask: 1 indicate this step is for updating node features
            flow_core_edge_mask: get the distributions we want to model in adjacency matrix
        Note: Copied from GraphAF with a minor extension.
        """
        # https://cs.stanford.edu/people/jure/pubs/graphrnn-icml18.pdf
        num_masks = (max_node_unroll # Number of nodes.
                    + ((max_edge_unroll - 1) * max_edge_unroll) // 2  # Number of elements under the diagonal. I.e. the number of possible bonds.
                    + (max_node_unroll - max_edge_unroll) * max_edge_unroll)

        num_mask_edge = num_masks - max_node_unroll
        node_masks1 = torch.zeros([max_node_unroll, max_node_unroll]).bool()
        adj_masks1 = torch.zeros([max_node_unroll, max_node_unroll, max_node_unroll]).bool()
        node_masks2 = torch.zeros([num_mask_edge, max_node_unroll]).bool()
        adj_masks2 = torch.zeros([num_mask_edge, max_node_unroll, max_node_unroll]).bool()

        # is_node_update_masks = torch.zeros([num_masks]).byte()

        link_prediction_index = torch.zeros([num_mask_edge, 2]).long()

        flow_core_edge_masks = torch.zeros([max_node_unroll, max_node_unroll]).bool()

        # masks_edge = dict()
        cnt = 0
        cnt_node = 0
        cnt_edge = 0
        for i in range(max_node_unroll):
            node_masks1[cnt_node][:i] = 1
            adj_masks1[cnt_node][:i, :i] = 1
            # is_node_update_masks[cnt] = 1
            cnt += 1
            cnt_node += 1
            edge_total = 0
            if i < max_edge_unroll:
                start = 0
                edge_total = i
            else:
                start = i - max_edge_unroll
                edge_total = max_edge_unroll
            for j in range(edge_total):
                if j == 0:
                    node_masks2[cnt_edge][:i + 1] = 1
                    adj_masks2[cnt_edge] = adj_masks1[cnt_node - 1].clone()
                    adj_masks2[cnt_edge][i, i] = 1
                else:
                    node_masks2[cnt_edge][:i + 1] = 1
                    adj_masks2[cnt_edge] = adj_masks2[cnt_edge - 1].clone()
                    adj_masks2[cnt_edge][i, start + j - 1] = 1
                    adj_masks2[cnt_edge][start + j - 1, i] = 1
                cnt += 1
                cnt_edge += 1


        assert cnt == num_masks, 'masks cnt wrong'
        assert cnt_node == max_node_unroll, 'node masks cnt wrong'
        assert cnt_edge == num_mask_edge, 'edge masks cnt wrong'

        cnt = 0
        for i in range(max_node_unroll):
            if i < max_edge_unroll:
                start = 0
                edge_total = i
            else:
                start = i - max_edge_unroll
                edge_total = max_edge_unroll

            for j in range(edge_total):
                link_prediction_index[cnt][0] = start + j
                link_prediction_index[cnt][1] = i
                cnt += 1
        assert cnt == num_mask_edge, 'edge mask initialize fail'

        for i in range(max_node_unroll):
            if i == 0:
                continue
            if i < max_edge_unroll:
                start = 0
                end = i
            else:
                start = i - max_edge_unroll
                end = i
            flow_core_edge_masks[i][start:end] = 1

        node_masks = torch.cat((node_masks1, node_masks2), dim=0)
        adj_masks = torch.cat((adj_masks1, adj_masks2), dim=0)


        flow_core_epsilon_masks = torch.zeros([max_node_unroll, num_mask_edge]).bool()  # This was not part of GraphAF.
        for i, length in enumerate(flow_core_edge_masks.sum(-1).cumsum(-1)):
            flow_core_epsilon_masks[i, :length] = 1

        return node_masks, adj_masks, link_prediction_index, flow_core_edge_masks, flow_core_epsilon_masks

    def __init__(self, in_channels=9, num_relations=3, hidden_size=128, max_graph_nodes=40, max_graph_diameter=12):
        super().__init__()
        self.in_channels = in_channels
        self.num_relations = num_relations

        self.max_graph_nodes = max_graph_nodes
        self.max_graph_diameter = max_graph_diameter

        # The masks can be made to be updated if they're of insufficient size. Otherwise they should be initialized
        #  just based on statistics from the dataset. I.e. max number of nodes and edges. The first argument is the
        # max number of nodes in a graph. The second is the maximum BFS depth. The max BFS depth is also the num_flow_layer
        node_mask, edge_mask, \
            link_prediction_index, flow_core_edge_masks,  \
                flow_core_epsilon_masks = self.initialize_masks(self.max_graph_nodes, self.max_graph_diameter)

        self.register_buffer('node_mask', node_mask)
        self.register_buffer('edge_mask', edge_mask)
        self.register_buffer('link_prediction_index', link_prediction_index)
        self.register_buffer('flow_core_edge_masks', flow_core_edge_masks)
        self.register_buffer('flow_core_epsilon_masks', flow_core_epsilon_masks)

        ## Add noise to GPU (Tried many things and this was the only one that worked)
        # TODO: Maybe organize the parameters better
        #self.sampling_parameters = nn.ParameterDict({
        #    'loc': nn.Parameter(torch.tensor(0, dtype=torch.float32), requires_grad=False),
        #    'scale': nn.Parameter(torch.tensor(1, dtype=torch.float32), requires_grad=False),
        #    'a': nn.Parameter(torch.tensor(0, dtype=torch.float32), requires_grad=False)}) # -math.sqrt(3)
        # self.sampling_parameters['b'] = nn.Parameter(-self.sampling_parameters['a'], requires_grad=False)
        self.zero = nn.Parameter(torch.tensor(0, dtype=torch.float32), requires_grad=False)
        self.one = nn.Parameter(torch.tensor(1, dtype=torch.float32), requires_grad=False)

        # We could have it go in +- math.sqrt(3). This would yield a variance of 1, and mean of 0, so we can easily
        #  check if the sampling_distribution matches the moments. Otherwise we could have it go
        self.deq_distribution = torch.distributions.Uniform(self.zero, self.one)
        self.sampling_distribution = torch.distributions.Normal(self.zero, self.one)

        #self.test = nn.Linear(in_channels, hidden_size) # FIXME: Finish RGCN and delete me.
        #self.rgcn = lambda x, adj: self.test(x)
        self.rgcn = RGCN(in_channels, hidden_size, num_relations)

        # GraphAF used TanH, and some sigmoid transform on the output followed by some kind of strictly positive "rescaling", in the st_nets.
        self.st_net_node = nn.Sequential(nn.Linear(hidden_size, hidden_size), nn.ReLU(),
                                         nn.Linear(hidden_size, 2 * self.in_channels))
        self.st_net_edge = nn.Sequential(nn.Linear(hidden_size * 3, hidden_size), nn.ReLU(),
                                         nn.Linear(hidden_size, 2 * (num_relations + 1)))

        # Flow++ would be interesting to look at: https://arxiv.org/pdf/1902.00275.pdf
        # ^ Very quickly this seems to mean applying flow transformations such that the normal distribution can be made
        #    to _conform_ better to the uniform distribution.
        # Would gumbel distribution be applicable instead of normal?

        # For use when multiple GPUs are in place (set this to false for now) I see that there are 4 free GPUs on sxm2sh
        # And they are not used that often - will look into this later
        self.mgpu = False

    def _embed_graph(self, node_emb):
        """
        :param x: features of shape (batch, repeat, N, num_feat)
        :return graph_emb: the graph embedding of shape (batch, repeat, num_feat)
        """

        aggr = 'add' # As was done in GraphAF.
        if aggr == 'add':
            # We probably want to use this. See https://arxiv.org/pdf/1810.00826.pdf
            # TODO: Qahir. Find optimum z1 prediction based on dataset statistics. Check if the generated z1 matches.
            return node_emb.sum(-2)  # (batch, repeat, d)
        elif aggr == 'mean':
            # Using mean pooling means features will be invariant to graph size, all this might amount to is a better
            # initialization for the Node/Edge-MLPs, since these can effectively learn the normalization constant.
            normalization = self.node_mask.sum(-1, keepdim=True)  # (repeat, N) -> (repeat, 1). Can be cached.
            normalization[0] = 1 # We start with zero nodes.
            return node_emb.sum(2) / normalization # (batch, repeat, d)
        elif aggr == 'max':
            # Max pool will probably not work well since it lacks a global context
            return node_emb.max(2)

    def _embed_node(self, x, adj_feat, graph_size):
        # Used for inference

        # We turn the one-hot into channels, so it's suitable for convolutions. We also drop the no-edge part.
        # adj_feat = adj_feat[..., 1:].transpose(-3, -1)  # (batch, 3, N, N)

        node_emb = self.rgcn(x.reshape(-1, *x.shape[2:]), adj_feat.reshape(-1, *adj_feat.shape[2:]))

        # Add mask to output - putting mask on output
        node_emb[:, graph_size:, :] = 0
        # node_emb[self.node_mask[graph_size].unsqueeze(0)]
        graph_emb_node = self._embed_graph(node_emb)
        return graph_emb_node

    def _embed_edge(self, x, adj_feat, graph_size, edge_index):
        # Used for inference
        node_emb = self.rgcn(x.reshape(-1, *x.shape[2:]), adj_feat.reshape(-1, *adj_feat.shape[2:]))
        node_emb[:, graph_size:, :] = 0  # Re-apply masking.
        # node_emb = node_emb.reshape(1,-1,self.max_graph_nodes,).squeeze()
        # assert graph_size not in [0, 1]
        # Set everything after where we are now to 0 - masking nodes

        h_i = self._embed_graph(node_emb) # (1, N)
        # FIXME: Verify correctness
        H_ii = node_emb[:, edge_index]
        H_ij = node_emb[:, graph_size-1]  # The new node
        # torch.gather(all_node_emb_edge, dim=2, index=[], sparse_grad=False)
        graph_emb_edge = torch.cat((h_i, H_ii, H_ij), axis=-2)
        return graph_emb_edge.reshape(*graph_emb_edge.shape[:-2], -1)



    def _embed_nodes(self, x, adj_feat: torch.FloatTensor):
        """
        :param x: features of shape (batch, N, num_feat)
        :param adj_feat: adjacency type matrix, one-hot encoded, of shape (batch, N, N, 4)
        :return: graph embeddings of shape (batch, N, d), edge embeddings of shape (batch, repeat-N, 3*d)
        """
        # Used for training.

        # We turn the one-hot into channels, so it's suitable for convolutions. We also drop the no-edge part.
        adj_feat = adj_feat[..., 1:].transpose(-3, -1) # (batch, 3, N, N)

        # We expand the masks to match the number of graphs in the batch.
        node_mask = self.node_mask[None, :, :, None].expand(x.shape[0], -1, -1, 1)  # (batch, repeat, N, num_feat or d)
        edge_mask = self.edge_mask[None, :, None, :, :].expand(adj_feat.shape[0], -1, # (batch, repeat, 3, N, N)
                                                               adj_feat.shape[1], -1, -1)

        # Apply masks.
        x = x[:, None, :, :].where(node_mask, x.new_zeros(1)) # (batch, repeat, N, num_feat)
        adj_feat = adj_feat[:, None, :, :].where(edge_mask, adj_feat.new_zeros(1)) # (batch, repeat, 3, N, N)

        # Transform using Relational Graph Neural Network.
        node_emb = self.rgcn(x.reshape(-1, *x.shape[2:]), adj_feat.reshape(-1, *adj_feat.shape[2:]))
        node_emb = node_emb.reshape(*x.shape[:-1], -1) # (batch*repeat, N, d) -> (batch, repeat, N, d)

        # If there's any bias in the layers of the R-GCN, then we have to re-mask these outputs. (Not done in GraphAF)
        # (Note it may be enough to only mask output, and not input. We would have to look into that..)
        node_emb = node_emb.where(node_mask, node_emb.new_zeros(1))

        # We can try different graph embedding methods. add or mean readout.
        graph_emb = self._embed_graph(node_emb)

        # The embedding of the nodes.
        graph_emb_node = graph_emb[:, :x.shape[2]]  # (batch, N, d). Relating to the full sub-graphs. eps_i.
        graph_emb_edge = graph_emb[:, x.shape[2]:, None, :] # (batch, repeat-N, 1, d)

        # The node embeddings relating to the partial sub-graphs. eps_ij:
        all_node_emb_edge = node_emb[:, x.shape[2]:] # (batch, repeat-N, N, 3)

        # Expand link index to match in batching dimension.
        index = self.link_prediction_index[None, :, :, None].expand(node_emb.shape[0], -1, # (batch, repeat-N, 2, d)
                                                                        -1, node_emb.shape[-1])

        # In the paper described as H_ii and H_ij. It is the node embeddings relating to the partial sub-graphs. eps_ij:
        node_emb_edge = torch.gather(all_node_emb_edge, dim=2, index=index, sparse_grad=False)  # (batch, repeat-N, 2, d)
        # Concat h_i onto it, so we have (h_i, H_ii, H_ij)
        graph_emb_edge = torch.cat([graph_emb_edge, node_emb_edge], dim=-2) # (batch, repeat, 3, d)
        # ^ Note we could also just concat in the last dimension all the features. They don't all need to be size d.
        # v Especially because we now reshape them to flatten the last two dimensions.
        return graph_emb_node, graph_emb_edge.reshape(*graph_emb_edge.shape[:-2], -1)

    def forward(self, x: torch.FloatTensor, adj_type: torch.LongTensor,
                graph_sizes: torch.LongTensor = None, verbose=False):
        """
        :param x: Graph feature matrix of size (batch, N, num_feat), with N < self.max_graph_nodes
        :param adj_type: Adjacency type matrix of shape (batch, N, N)
        :param graph_sizes: The size of the individual graphs, size (batch, )
        :param verbose: Returns the likelihood and epsilon for every step of generation.
        :return log_likelihood: Log likelihood of the graphs, size (batch, )

        Both x and adj_type must be zero-padded to match self.max_graph_nodes.
        This should happen in the collate_fn of the dataloader.
        """
        # Idea: We should try canonical vs uniform initial node.
        # Idea: We should try adding a node, corresponding to _no node_ meaning the graph generation is over.
        # Idea: We should try to investigate Flow++ for the sampling distribution.
        # TODO: Make sure masks are not too small?

        # Convert adj to one-hot. Four features for the bond types [no bond, single, double, triple]
        adj_one_hot = adj_type.new_zeros(adj_type.shape + (self.num_relations + 1,), dtype=torch.float)  # (batch, N, N, 4)
        adj_one_hot.scatter_(value=1, dim=-1, index=adj_type[..., None]) # We could also try using nn.functional.one_hot

        # Get graph embeddings
        node_emb, edge_emb = self._embed_nodes(x, adj_one_hot) # (batch, N, d), (batch, repeat-N, 3*d)

        # Take out the elements under the diagonal. (Since the future edge values can't influence past.)
        adj_one_hot = adj_one_hot[:, self.flow_core_edge_masks, :] # (batch, repeat-N, 4)

        # Dequantized versions, for Flow, so we can model a discrete variable with continuous flow. Also called z.
        if self.training:
            x_deq = x + self.deq_distribution.rsample(x.shape)
            adj_deq = adj_one_hot + self.deq_distribution.rsample(adj_one_hot.shape)
        else:
            # Should we use a deterministic dequantization for validation?
            x_deq = x
            adj_deq = adj_one_hot
            # We could add self.deq_distribution.mean, or maybe x_deq = x_deq * 2, to really propagate signal.

        # Maybe a softmax output (with a factor 2, to span [0, 2)) would be appropriate for t? or inverse softmax transform before giving input?
        # ^ This seems to be what they try to do in their code, with the sigmoid_shift=2!
        # sigma = torch.exp(log_var/2), log_var=log_s, t=mu.
        node_log_s, node_t = torch.chunk(self.st_net_node(node_emb), 2, dim=-1)
        node_eps = (x_deq - node_t) * torch.exp(-node_log_s)  # eps_i.

        edge_log_s, edge_t = torch.chunk(self.st_net_edge(edge_emb), 2, dim=-1)

        adj_eps = (adj_deq - edge_t) * torch.exp(-edge_log_s)  # eps_ij. Note: Different from GraphAF ( they do (x_deq+t)*log_s.exp() )

        # Calculate the likelihood of our sample. By the change of variables formula we simply add the log Jacobian.
        log_likelihood_node = self.sampling_distribution.log_prob(node_eps) - node_log_s
        log_likelihood_edge = self.sampling_distribution.log_prob(adj_eps) - edge_log_s
        log_likelihood_node = log_likelihood_node.sum(-1)  # We sum in feature dimension. This is because our prior is
        log_likelihood_edge = log_likelihood_edge.sum(-1)  # a multivariate unit normal distribution over this space.

        if verbose:
            return (node_eps, adj_eps), (log_likelihood_node, log_likelihood_edge)

        # They write in the GraphAF code "TOD0: add mask for different molecule size, i.e. do not model the distribution
        #  over padding nodes.", and I could not agree more, so lets do that :)

        # The way GraphAF works is that generation is stopped when the next node is not connected to the sub-graph.
        node_mask = self.node_mask[graph_sizes, :]  # (batch, N)
        edge_mask = self.flow_core_epsilon_masks[graph_sizes + 1]
        # Zero out generated nodes after the final, log(1) = 0, so this corresponds to no error, as we simply don't care
        # Then we multiply the probabilities p(x1,x2,...) = p(x1)*p(x2|x1)*...*1*1*... by summing over the log probs.
        log_likelihood_node = torch.where(node_mask, log_likelihood_node, self.zero).sum(1)
        log_likelihood_edge = torch.where(edge_mask, log_likelihood_edge, self.zero).sum(1)

        if True:
            # Length-normalize probabilities.
            # This corresponds to the geometric average of p(x1,x2,x3), i.e. the averaged probability of any epsilon
            #  in the sequence. The result is bigger graphs are not weighed higher in the loss.
            idx = graph_sizes != 0  # Operation not well-defined for zero size graph. Regardless, in this case, prob=1.
            normalization = node_mask.sum(-1) + edge_mask.sum(-1)
        else:
            normalization = 1

        # Best likelihood is where every node/edge is predicted perfectly, meaning that the dequantized data, when
        #  transformed back to the normal-distribution space is placed at the mode of the normal PDF.
        # In this case

        # The final probability is the product of all p(eps_i) and p(eps_ij)'s. Return negative log likelihood.
        return -( log_likelihood_node + log_likelihood_edge ) / normalization



    def reverse(self, x, adj, latent, mode, graph_size, edge_index=None): # The last two are for edges
        '''
        :param x: generated subgraph node features (1, N, 9)
        :param adj: generated subgraph adjecency (1, 4, N, N)
        :param latent: sample latent vector with shape (1, 1, 9) or (1, 4)
        :param mode If mode == 0 generate new node, mode == 1 generate new edge:
        :return:
        '''

        # Reshape adj for embedding
        # adj = adj.reshape(-1,self.max_graph_nodes,self.max_graph_nodes,4)
        # emb_node, emb_graph = self._embed_nodes(x,adj)
        # Checks
        #assert mode == 1 and edge_index is not None, 'Specify edge_index for mode == 0'
        assert x.size(0) == 1
        assert adj.size(0) == 1
        #assert edge_index is None or (edge_index.size(0) == 1 and edge_index.size(1) == 2)

        if mode == 0:
            # For node generation.
            node_emb = self._embed_node(x, adj, graph_size)
            logs_t = self.st_net_node(node_emb)
        elif mode == 1:
            # For edge generation.
            edge_emb = self._embed_edge(x, adj, graph_size, edge_index)
            logs_t = self.st_net_edge(edge_emb)
        log_s, t = torch.chunk(logs_t, 2, dim=-1)

        return latent * torch.exp(log_s) + t

    def log_prob(self, x: torch.FloatTensor, adj_type: torch.LongTensor, sizes=None):
        return -self.forward(x, adj_type, sizes)

    def rsample(self, cur_node_features, cur_adj_features, max_atoms=38, temperature=1):
        rw_mol = Chem.RWMol()
        mol = None

        # TODO: put somewhere else - we need a lookup shared between dataloader (to encode) and model (to decode)
        peridoc_table = Chem.rdchem.GetPeriodicTable()
        atoms = ['C', 'N', 'O', 'F', 'P', 'S', 'Cl', 'Br', 'I']  # FIXME: Duplicate of line in dataloader. This MUST be the reverse of the mapping defined there.
        bonds = [Chem.rdchem.BondType.SINGLE, Chem.rdchem.BondType.DOUBLE, Chem.rdchem.BondType.TRIPLE]

        # Derived mappings
        num2symbol = {i: atom for i, atom in enumerate(atoms)}
        num2atom = {i: peridoc_table.GetAtomicNumber(atom) for i, atom in enumerate(atoms)} # Some places it's GetAtomicNum, others its GetAtomicNumber, really RDLit?
        num2bond = {i+1: bond for i, bond in enumerate(bonds)}

        total_resample = 0
        each_node_resample = np.zeros(cur_node_features.shape[:-1])
        # Sample a node
        for i in range(max_atoms):
            # Reverse flow for sampling nodes.
            # We sample an e_i
            latent_node = self.sampling_distribution.sample((cur_node_features.shape[0], 1,
                                                             cur_node_features.shape[-1])) # (1, node_types)
            latent_node = latent_node * temperature # Apply temperature

            # Transform e_i to z_i
            latent_node = self.reverse(cur_node_features, cur_adj_features, latent_node, mode=0, graph_size=i)
            feature_id = torch.argmax(latent_node[:, 0], dim=-1, keepdim=True)  # FIXME: Is rsample valid? This is non-differentiable. One could return the raw values in stead. This would allow us to get probability w.r.t. the uniform density.
            cur_node_features[:, i].scatter_(value=1, dim=-1, index=feature_id)
            # cur_adj_features[0, i, i, :] = 1.0
            atom_idx = rw_mol.AddAtom(Chem.Atom(num2atom[feature_id.item()]))  # Works since there's only one.

            if i < 1:
                # We need at least two atoms to generate edges between. Note: Not done in GraphAF code.
                continue
            elif i < self.max_graph_diameter: # Max edge unroll based on paper
                # Generate edges now
                edge_total = i # Edge to sample for current node
                start = 0
            else:
                edge_total = self.max_graph_diameter
                start = i - self.max_graph_diameter

            is_connected = False
            for j in range(edge_total):
                # The set of edge types we have tried to produce, which resulted in invalid molecules.
                invalid_bond_type_set = set()
                resample_edge = 0

                # print(j + start, i)  # Values similar to link_prediction_index.

                valid = False
                while not valid:
                    if len(invalid_bond_type_set) == len(num2bond):
                        # print('There is no way to generate valid bonds from this mol. We have tried all types.')
                        # In this case, it is like edge_discrete_id = 0, since we skip over bond-creation.
                        # We hope that some other atom in the current subgraph can bond with this.
                        break
                    if resample_edge > 50:
                        print('After 50 tries, we could not generate a bond. We tried types:', invalid_bond_type_set)
                        break

                    latent_edge = self.sampling_distribution.rsample((cur_adj_features.shape[0], self.num_relations + 1))
                    latent_edge = self.reverse(cur_node_features, cur_adj_features, latent_edge, graph_size=i+1, mode=1, edge_index=j + start)
                    edge_discrete_id = torch.argmax(latent_edge, -1).item()

                    # Check validity
                    if edge_discrete_id in invalid_bond_type_set:
                        # Have we already confirmed this is invalid?
                        # valid = False
                        pass
                    elif edge_discrete_id == 0:
                        # 0 means no bond.
                        valid = True
                    else:
                        # Check validity of adding this bond.
                        rw_mol.AddBond(i, j + start, num2bond[edge_discrete_id])
                        valid = check_valency(rw_mol)
                        if not valid:
                            # Not valid. Undo.
                            rw_mol.RemoveBond(i, j + start)
                            invalid_bond_type_set.add(edge_discrete_id)
                        else:
                            cur_adj_features[0, edge_discrete_id-1, i, j + start] = 1
                            cur_adj_features[0, edge_discrete_id-1, j + start, i] = 1
                            is_connected = True

                    total_resample += 1.0
                    each_node_resample[0, i] += 1.0
                    resample_edge += 1
                if not valid:
                    print('Failed to create any valid bonds!')

            if not is_connected:
                rw_mol.RemoveAtom(atom_idx)  # Undo the last added atom.
                mol = rw_mol.GetMol()
                print('Molecule generation done.', Chem.MolToSmiles(mol))
                break

        assert mol is not None, 'mol is None...'

        final_valid = check_valency(mol)
        assert final_valid is True, 'Warning final valid failed'

        final_mol = convert_radical_electrons_to_hydrogens(mol)
        smiles = Chem.MolToSmiles(final_mol, isomericSmiles = True)
        assert '.' not in smiles, 'Warning final molecule is donnected'

        final_mol = Chem.MolFromSmiles(smiles)

        num_atoms = final_mol.GetNumAtoms()
        num_bonds = final_mol.GetNumBonds()

        print()

        pure_valid = 0
        if total_resample == 0:
            pure_valid = 1.0

        return smiles, pure_valid, num_atoms

    # Temperature is usually a parameter that decreases the variance as time goes on
    def sample(self, temperature = 0.75):  # FIXME: Default temp 1.0? (To stay true)

        # One idea is that we could do many samplings, then get the distribution of only the valid subset of bonds,
        #  then finally sample from a discrete distribution with these weights.

        cur_node_features = self.sampling_distribution.loc.new_zeros([1, self.max_graph_nodes, self.in_channels])
        cur_adj_features = self.sampling_distribution.loc.new_zeros([1, self.num_relations,
                                                                     self.max_graph_nodes, self.max_graph_nodes])

        with torch.no_grad():
            return self.rsample(cur_node_features, cur_adj_features, temperature=temperature)


### Helper functions

def mol2graph(smiles='c1ccccc1'):
    m = Chem.MolFromSmiles(smiles)
    Chem.Kekulize(m)
    # If adj is not kekulized, then some entries in adj may be 1.5.
    adj = torch.as_tensor(Chem.GetAdjacencyMatrix(m, useBO=True), dtype=torch.long)
    # torch_sparse.SparseTensor.from_dense(adjmat)

    # We can permute inputs randomly when training.
    # Then when predicting we can predict from different permutations and average predictions, i.e. model averaging.
    #  Meaning on the third atom prediction, we can try permute the two previous.

def check_valency(mol):
    """
    Checks that no atoms in the mol have exceeded their possible
    valency
    :return: True if no valency issues, False otherwise
    """
    try:
        Chem.SanitizeMol(mol,
                         sanitizeOps=Chem.SanitizeFlags.SANITIZE_PROPERTIES)
        return True
    except ValueError:
        return False

def check_chemical_validity(mol):
    """
    Checks the chemical validity of the mol object. Existing mol object is
    not modified. Radicals pass this test.
    :return: True if chemically valid, False otherwise
    """
    s = Chem.MolToSmiles(mol, isomericSmiles=True)
    m = Chem.MolFromSmiles(s)  # implicitly performs sanitization
    if m:
        return True
    else:
        return False

def convert_radical_electrons_to_hydrogens(mol):
    """
    Converts radical electrons in a molecule into bonds to hydrogens. Only
    use this if molecule is valid. Results a new mol object
    :param mol: rdkit mol object
    :return: rdkit mol object
    """
    m = copy.deepcopy(mol)
    if Chem.Descriptors.NumRadicalElectrons(m) == 0:  # not a radical
        return m
    else:  # a radical
        print('converting radical electrons to H')
        for a in m.GetAtoms():
            num_radical_e = a.GetNumRadicalElectrons()
            if num_radical_e > 0:
                a.SetNumRadicalElectrons(0)
                a.SetNumExplicitHs(num_radical_e)
    return m




if __name__ == "__main__":
    from rdkit import Chem
    m = Chem.MolFromSmiles('c1ccccc1')
    Chem.Kekulize(m)
    import torch_sparse

    adjmat = torch.as_tensor(Chem.GetAdjacencyMatrix(m, useBO=True), dtype=torch.long) #, dtype=torch.float)
    adjmat = torch_sparse.SparseTensor.from_dense(adjmat)

    #edge_index, edge_weight = geom.utils.dense_to_sparse(adjmat)

    # from the nevae code, the edge weight is actually the bond type, i.e. 1, 1.5, 2, 3, ...
    # it should really just be a one-hot featureized vector, which would be applied in the messaging step.

    #print(edge_weight.shape, edge_index.shape)

    # atoms = ['C', 'H', 'O', 'N']
    atoms = ['C', 'N', 'O', 'F', 'P', 'S', 'Cl', 'Br', 'I']
    atoms = {C:i for i, C in enumerate(atoms)}
    nodes = torch.as_tensor( [atoms[atom.GetSymbol()] for atom in m.GetAtoms()], dtype=torch.long )
    x = nn.functional.one_hot(nodes, num_classes=len(atoms)).float()
    #(edge_weight, x.shape)

    #conv1 = RGCNConv(x.shape[-1], 2, 3, 1)
    #print(conv1(x, adjmat, adjmat.value-1))

    graphsize = 40
    xpadded = x.new_zeros((1, graphsize, x.shape[1]))
    xpadded[0, :x.shape[0], :x.shape[1]] = x
    MAF()(xpadded, adjmat.sparse_resize([graphsize, graphsize]).to_dense()[None], torch.LongTensor((6,)))

    g = MAF()

    g.log_prob(xpadded, adjmat.sparse_resize([graphsize, graphsize]).to_dense()[None], torch.LongTensor((6,)))


    '''' 
    exit()
    conv2 = GraphAFConv(x.shape[-1], 2, 3, 1)
    print(conv2(x, adjmat, adjmat.storage._value-1))

    net = RGCN(x.shape[-1])
    print(net(x, adjmat, adjmat.storage._value-1).shape)
    '''
